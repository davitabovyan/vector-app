import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { AuthService } from 'src/app/shared/service/rest-api/auth.service'
import { WorkAreaComponent } from './shared/component/work-area/work-area.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'vector-app';
  state: string[] = [];
  public loggedIn: boolean | "" | null;

  
  @ViewChild('workArea') workArea: WorkAreaComponent;
  
  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.loggedIn = this.authService.isLoggedIn();
  }


  onSelect(page: string): void {
    this.state.push(page);
    this.workArea.loadComponent(page);
  }

}
