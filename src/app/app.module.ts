import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';

// import {MatAutocompleteModule} from '@angular/material/autocomplete';
// import {MatBadgeModule} from '@angular/material/badge';
// import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
// import {MatButtonToggleModule} from '@angular/material/button-toggle';
// import {MatCardModule} from '@angular/material/card';
// import {MatChipsModule} from '@angular/material/chips';
// import {MatStepperModule} from '@angular/material/stepper';
// import {MatDividerModule} from '@angular/material/divider';
// import {MatExpansionModule} from '@angular/material/expansion';
// import {MatGridListModule} from '@angular/material/grid-list';
// import {MatListModule} from '@angular/material/list';
// import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
// import {MatPaginatorModule} from '@angular/material/paginator';
// import {MatProgressBarModule} from '@angular/material/progress-bar';
// import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
// import {MatRadioModule} from '@angular/material/radio';
// import {MatSidenavModule} from '@angular/material/sidenav';
// import {MatSliderModule} from '@angular/material/slider';
// import {MatSlideToggleModule} from '@angular/material/slide-toggle';
// import {MatSnackBarModule} from '@angular/material/snack-bar';
// import {MatToolbarModule} from '@angular/material/toolbar';
// import {MatTreeModule} from '@angular/material/tree';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopMenuComponent } from './shared/component/top-menu/top-menu.component';
import { AuthInterceptor } from './shared/interceptor/authInterceptor';
import { ControlPanelComponent } from './shared/component/control-panel/control-panel.component';
import { WorkAreaComponent } from './shared/component/work-area/work-area.component';
import { LogInComponent } from './shared/component/log-in/log-in.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import { GenericTable } from './shared/component/tables/generic/generic-table';
import { HostDirective } from './shared/directive/host.directive';
import { LoaderComponent } from './shared/component/parts/loader/loader.component';
import { PersonFormComponent } from './shared/component/forms/model/person/person.form.component';
import { CellComplexDataPipe } from './shared/pipes/cell.complex.data.pipe';
import { TableFooterDataPipe } from './shared/pipes/table.footer.data.pipe';
import { DepartmentFormComponent } from './shared/component/forms/model/department/department.form.component';
import { TypeAheadTable } from './shared/component/tables/typeahead/typeahead';
import { RoleFormComponent } from './shared/component/forms/model/role/role.form.component';
import { ContractFormComponent } from './shared/component/forms/model/contract/contract.form.component';
import { FormatNumberPipe } from './shared/pipes/format.number.pipe';
import { VacationFormComponent } from './shared/component/forms/absence/vacation/vacation.form.component';
import { TiemsheetFormComponent } from './shared/component/forms/model/timesheet/timesheet.form.component';
import { ConverterService } from './shared/service/utils/converter.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'L',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    ControlPanelComponent,
    WorkAreaComponent,
    LogInComponent,
    GenericTable,
    TypeAheadTable,
    LoaderComponent,
    PersonFormComponent,
    DepartmentFormComponent,
    RoleFormComponent,
    ContractFormComponent,
    VacationFormComponent,
    TiemsheetFormComponent,
    CellComplexDataPipe,
    TableFooterDataPipe,
    FormatNumberPipe,
    HostDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // Material
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    DragDropModule,
    MatTableModule,
    PortalModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatTabsModule,
    MatDialogModule,
    MatSortModule,
    MatCheckboxModule,

    // MatAutocompleteModule,
    // MatBadgeModule,
    // MatBottomSheetModule,
    // MatButtonToggleModule,
    // MatCardModule,
    // MatChipsModule,
    // MatStepperModule,
    // MatDividerModule,
    // MatExpansionModule,
    // MatGridListModule,
    // MatListModule,
    // MatNativeDateModule, MatRippleModule,
    // MatPaginatorModule,
    // MatProgressBarModule,
    // MatProgressSpinnerModule,
    // MatRadioModule,
    // MatSidenavModule,
    // MatSliderModule,
    // MatSlideToggleModule,
    // MatSnackBarModule,
    // MatToolbarModule,
    // MatTreeModule,
  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'hy-AM'},
    ConverterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
 