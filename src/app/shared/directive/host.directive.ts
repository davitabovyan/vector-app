import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[ihost]',
  })
  export class HostDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
  }