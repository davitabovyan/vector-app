import { Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WindowControllService {
    zIndex = 1;
    typeAhead: string;
      
  public focusWindow(id: number): void {
    this.zIndex = this.zIndex + 1;
    let openWindow = document.getElementById(`${id}`);
    if (openWindow == null) {
      throw new Error(`No open window with id ${id}`);
    } 
    openWindow.style.zIndex = '' + this.zIndex;
  }

  public isCurrentActiveWindow(id: number): boolean {
    let openWindow = document.getElementById(`${id}`);
    if (openWindow == null) {
      throw new Error(`No open window with id ${id}`);
    } 
    return openWindow.style.zIndex == '' + this.zIndex;
  }
}