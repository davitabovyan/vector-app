import { Injectable, Type } from '@angular/core';
import { TopMenuService } from '../top-manue.service';
import { MenuItem } from '../../dto/menu.item';
import { TableWindowData } from '../../dto/table.window.data';
import { FormWindowData } from '../../dto/form.window.data';
import { GenericTable } from '../../component/tables/generic/generic-table';
import { FormDataInterface } from '../../dto/form.data.interface';

@Injectable({
  providedIn: 'root'
})
export class WindowService {
    private openedWindowsNumber = 0;
    private idToIndexMapping = new Map<number,number>();

    constructor(private topMenuService: TopMenuService) {}

    getTableWindowDataByKey(page: string): TableWindowData {
        const item = this.getMenuItemByKey(page);
        if(item.type == null) {
            throw new Error(`Item type is not set for selectable menu item with key ${page}`);
        }
        const id = this.getRandomId();
        const windowData = new TableWindowData((<Type<GenericTable>>item.type), item.size, item.arm, this.openedWindowsNumber, id);
        this.updateWindowIds(id);
        return windowData;
    }

    getFormWindowDataByKey(page: string): FormWindowData {
        const item = this.getMenuItemByKey(page);
        if(item.type == null) {
            throw new Error(`Item type is not set for selectable menu item with key ${page}`);
        }
        const id = this.getRandomId();
        const windowData = new FormWindowData((<Type<FormDataInterface>>item.type), item.size, item.arm, this.openedWindowsNumber, id, page);
        this.updateWindowIds(id);
        return windowData;
    }

    getOpenWindowIndexForRemove(id: number): number {
        const currentIndex = this.idToIndexMapping.get(id);
        if(currentIndex == undefined) throw new Error(`No window index for id ${id}`);
        this.reorderOpenWindowOnDelete(id, currentIndex);
        --this.openedWindowsNumber;
        return currentIndex;
    }

    private updateWindowIds(id: number) {
        this.idToIndexMapping.set(id, this.openedWindowsNumber);
        this.openedWindowsNumber++;
    }

    private getRandomId(): number {
        return Math.floor((Math.random()*1000));
    }

    private reorderOpenWindowOnDelete(id: number, index: number) {
        this.idToIndexMapping.delete(id);
        for(const [key, value] of this.idToIndexMapping) {
            if(value > index) this.idToIndexMapping.set(key, value - 1);
        }
    }

    private getMenuItemByKey(key: string): MenuItem {
        const menuItems: MenuItem[] = this.topMenuService.menuItems;
        let found = this.recursionOnForMenuItem(key, menuItems);
        if(found == null) {
            throw new Error(`No menu matching menu item with key ${key}`)
        }
        return found;
    }

    private recursionOnForMenuItem(key: string, items: MenuItem[]): MenuItem | null {
        for(let item of items) {
            if(item.inner.length == 0){
                if(item.key === key){
                    return item;
                } else {
                    continue;
                }
            } else {
                let temp = this.recursionOnForMenuItem(key, item.inner);
                if(temp != null) {
                    return temp;
                } else {
                    continue;
                }
            }
        }
        return null;
    }
}