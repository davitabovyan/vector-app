// import { Injectable, ChangeDetectorRef } from '@angular/core';
// import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
// import { MatTableDataSource } from '@angular/material/table';

// @Injectable({
//     providedIn: 'root'
//   })
// export class LeftMenuService {
//     constructor(public dialog: MatDialog) {
    
//       }
    
//     public openMenu(height: string, width: string, event: MouseEvent,
//        uuid: string, type: string, template: any, dataSource: MatTableDataSource<any>, changeDetector: ChangeDetectorRef) {

//         let config = new MatDialogConfig();
//         config.backdropClass = 'no-class';
//         config.autoFocus = true;
//         config.restoreFocus = true;
//         config.closeOnNavigation = true;
//         config.height = height;
//         config.width = width;
//         config.position = {top: event.clientY + 'px', left: event.clientX + 'px'};
//         config.data = {uuid: uuid, type: type, dataSource: dataSource, changeDetector: changeDetector};
//         this.dialog.open(template, config);
//     }
// }