import { Injectable } from '@angular/core';
import { ObjCodeEnum } from '../../enums/objcode.enum';

import orderBy from 'lodash/orderBy';

@Injectable({
    providedIn: 'root'
})
export class FormService {

    public getNextCode(objCode: ObjCodeEnum): string {
        let array = JSON.parse(sessionStorage.getItem(objCode) || "");
        array = orderBy(array, ['code'], ['desc']);
        let length = this.getLength(objCode);
        return array.length != 0 ? this.increment(array[0].code, length) : '0001';
    }

    private increment(code: string, length: number): string {
        let current = parseInt(code,10) + 1;
        let temp = current+"";
        let size = temp.length;
        while(size++<length) {
            temp = "0" + temp;
        }
        return temp;
    }

    private getLength(objCode: ObjCodeEnum): number {
        switch(objCode) {
            case ObjCodeEnum.PERSON:
            case ObjCodeEnum.ROLE:
                return 4;
            case ObjCodeEnum.DEPARTMENT:
                return 3;
        }
        throw new Error(`incorrect objcode ${objCode}`);
    }
}