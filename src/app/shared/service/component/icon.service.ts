import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
  })
export class IconService {

    private readonly PATH = 'assets/svg/icon';
    private icons = ['avarage','bank','calculate','cash','contract','currency','department','empty',
      'lists','management','ml','new','orgchart','payslip','person','reload','role','settings','sign-out','sign-out','statistical','sl',
      'tax','timesheet','vacation','tail-spin'];

    constructor(private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer) {

    }

    public register() {
        for(let icon of this.icons) {
            this.iconRegistry.addSvgIcon(icon, this.sanitizer.bypassSecurityTrustResourceUrl(`${this.PATH}/${icon}.svg`));
        }
    }

    public registerSpinner() {
        this.iconRegistry.addSvgIcon('loading', this.sanitizer.bypassSecurityTrustResourceUrl(`${this.PATH}/tail-spin.svg`));
    }
}