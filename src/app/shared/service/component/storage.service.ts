import { Injectable } from '@angular/core';
import { ObjCodeEnum } from '../../enums/objcode.enum';

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';

import { ContractModel } from '../../model/contract.model';
import { DepartmentModel } from '../../model/department.model';
import { PersonModel } from '../../model/person.model';
import { BaseModel } from '../../model/base.model';
import { RoleModel } from '../../model/role.model';
import { ConverterService } from '../utils/converter.service';

const moment = _rollupMoment || _moment;

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(private converterService:ConverterService){
    }

    public setPayrollCurrentMonth() {
        let currentPeriod = moment().subtract(1,'M').format('MMYY');
        sessionStorage.setItem("pCurrMonth", currentPeriod);
    }

    public createPayrollPeriods() {
        let periods : any[] = [];
        for(let i=1;i<=12;++i) {
            let period = moment().subtract(i,'M').format('MMYY'); 
            let object = {"code":period,"name":this.converterService.parsePeriod(period)};
            periods.push(object);
        }
        this.setPayrollCurrentMonth();
        sessionStorage.setItem("period", JSON.stringify([...periods]));
    }

    public updateTypeahead(objCode: ObjCodeEnum, data: any[]) {
        switch(objCode) {
            case ObjCodeEnum.DEPARTMENT:
            case ObjCodeEnum.ROLE: 
                data = data.map(({uuid, code, name})=>({uuid, code, name}));
            break;
            case ObjCodeEnum.PERSON: 
                data = data.map(({uuid, code, firstName, lastName, middleName})=>({uuid, code, firstName, lastName, middleName}));
            break;
        }
        sessionStorage.setItem(objCode, JSON.stringify(data));
    }

    public appendDependences(objCode: ObjCodeEnum, data: any[]) : any[] {
        switch(objCode) {
            case ObjCodeEnum.PERSON:
            case ObjCodeEnum.DEPARTMENT:
            case ObjCodeEnum.ROLE:
            case ObjCodeEnum.TIMESHEET:
                return data;
            case ObjCodeEnum.CONTRACT:
                let result: ContractModel[] = [];
                let departments = <DepartmentModel[]> JSON.parse(sessionStorage.getItem(ObjCodeEnum.DEPARTMENT) || "");
                let persons = <PersonModel[]> JSON.parse(sessionStorage.getItem(ObjCodeEnum.PERSON) || "");
                let roles = <RoleModel[]> JSON.parse(sessionStorage.getItem(ObjCodeEnum.ROLE) || "");
                for( let model of <ContractModel[]> data) {
                    result.push(this.appendContractDependncy(model, departments, persons, roles));                   
                }
                return result;
            break;
        }
        throw new Error(`incorrect objcode ${objCode}`);
    }

    public appendContractDependncy(model: ContractModel, departments: DepartmentModel[],
         persons: PersonModel[], roles: RoleModel[]) : ContractModel {

        model.department = <DepartmentModel> this.getObjectByUUID(model.department.uuid, departments);
        model.person = <PersonModel> this.getObjectByUUID(model.person.uuid, persons);
        model.role = <RoleModel> this.getObjectByUUID(model.role.uuid, roles);
        return model;
    }

    private getObjectByUUID(uuid: string, list: BaseModel[]) : BaseModel {
        for(let model of list) {
            if(model.uuid === uuid) return model;
        }
        throw new Error(`no object with uuid ${uuid} in storage`);
    }
}