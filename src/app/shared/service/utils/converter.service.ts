import { Injectable } from '@angular/core';
import { ObjCodeEnum } from '../../enums/objcode.enum';

import forEach from 'lodash/forEach';

@Injectable({
    providedIn: 'root'
  })
export class ConverterService {

  public formatDay(day: number) : string {
    if(day < 0 && day > 31) throw new Error(`Incorrect day number: ${day}`);
    switch(day){
      case 25: return "Ա";
      case 26: return "Հ";
      case 27: return "Հ";
      case 28: return "Չ";
      case 29: return "";
      case 30: return "X";
      case 31: return "Տ";
      default: return ""+day;
    }
  }

  public convertData(objCode: string, data: any[]) : any[] {
    switch (objCode) {
      case ObjCodeEnum.TIMESHEET:
        forEach(data, function(object) {
          forEach(object["hours"], function(value, key) {
            let column = 'day' + key.split("-")[2];
            object[column] = value;
          });
        });
        break;

      default: 
    }
    return data;
  }

  public parseDateToPeriod(date : Date) : string {
    let mm = String(date.getMonth() + 1).padStart(2, '0');
    let yy = (date.getFullYear() + "").substr(2);
    return mm + yy;
  }

  public parsePeriod(period : string | null) : string {
    if(period == null || period.length != 4) throw new Error(`Period ${period} can't be parsed`);
    let year = "20" + period.substr(2);
    let month;
    switch(period.substr(0,2)){
      case "01": month = "Հունվար";
      break;
      case "02": month = "Փետրվար";
      break;
      case "03": month = "Մարտ";
      break;
      case "04": month = "Ապրիլ";
      break;
      case "05": month = "Մայիս";
      break;
      case "06": month = "Հունիս";
      break;
      case "07": month = "Հուլիս";
      break;
      case "08": month = "Օգոստոս";
      break;
      case "09": month = "Սեպտեմբեր";
      break;
      case "10": month = "Հոկտեմբեր";
      break;
      case "11": month = "Նոյեմբեր";
      break;
      case "12": month = "Դեկտեմբեր";
      break;
      default: new Error(`Incorrect period format: ${period}`);
    }
    return month + " " + year;
  }

  public formatNumber(number: any) : string {
      if(number === undefined) return '';
      let stringify = '' + number;
      let fraction = stringify.slice(-2);
      let integer = stringify.slice(0, stringify.length - 2);
      let q = Math.ceil(integer.length/3);
      var num = new Array();
      num[0] = integer.slice(-3);
      var txt = num[0];
      for(let i=1;i<q;i++){
          num[i] = integer.slice(-3*i-3,-3*i);
          txt = num[i] + "," + txt;
      }
      return txt + "." + fraction;
  }

  public stringifyObject(row: any) : string {
      let str = '';
      for (let [key, value] of Object.entries(row)) {
        if (key === 'uuid' || key === 'code') {
          continue;
        } else {
          str += value;
          str += ' ';
        }
      }
      return str;
  }
}