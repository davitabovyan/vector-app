import { Injectable } from '@angular/core';

import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
import { DurationStatistics } from '../../dto/duration.statistics';

const moment = _rollupMoment || _moment;

@Injectable({
    providedIn: 'root'
  })
export class DateService {

    public calculateEndDate(start: Moment, numOfWorkingDays: number, fiveDaysWeek: boolean) : Moment {
        let date = start.clone();
        while(numOfWorkingDays > 0) {
            date = date.add(1,"d");
            if(date.isoWeekday() == 7 || (fiveDaysWeek && date.isoWeekday() == 6)) continue;
            numOfWorkingDays--;
        }
        return date;
    }

    public calculateDuration(start: Moment, end: Moment, fiveDaysWeek: boolean) : DurationStatistics {
        let statistics = new DurationStatistics();
        let date = start.clone();
        while(end.isAfter(date)) {
            date = date.add(1,"d");
            statistics.total++;
            if(date.isoWeekday() == 7){
                statistics.sunday++;
                continue;
            } else if(date.isoWeekday() == 6) {
                statistics.suturday++
                if(fiveDaysWeek) continue;
            }
            let holiday = this.isHoliday(date);
            if(holiday != undefined){
                statistics.holiday++;
                statistics.holidayNames.push(holiday);
                continue;
            }  
            statistics.working++;
        }
        return statistics;
    }

    public isHoliday(day: Moment) : string | undefined {
        let date = day.clone().utc().startOf('day');
        let month = date.month();
        let monthDay = date.date();

        switch(month) {
            case 0:
                if(monthDay < 3) return "Ամանոր";
                else if(monthDay < 6) return "Նախածննդյան տոներ";
                else if(monthDay == 6) return "Սուրբ Ծնունդ և Հայտնություն";
                else if(monthDay == 7) return "Մեռելոց";
                else if(monthDay == 28) return "Բանակի օր";
                break;
            case 2:
                if(monthDay == 8) return "Կանանց տոն";
                break;
            case 3:
                if(monthDay == 24) return "Ցեղասպանության զոհերի հիշատակի օր";
                break;
            case 4:
                if(monthDay == 1) return "Աշխատանքի օր";
                else if(monthDay == 9) return "Հաղթանակի և խաղաղության տոն";
                else if(monthDay == 28) return "Հանրապետության տոն";
                break;
            case 6:
                if(monthDay == 5) return "Սահմանադրության օր";
                break;
            case 8:
                if(monthDay == 21) return "Անկախության տոն";
                break;
            case 12:
                if(monthDay == 21) return "Ամանոր";
                break;
        }

        return undefined;
    }
}