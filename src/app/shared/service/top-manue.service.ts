import { Injectable } from '@angular/core';
import { MenuItem } from '../dto/menu.item';
import { WindowSize } from '../enums/window.size';
import { GenericTable } from '../component/tables/generic/generic-table';
import { PersonFormComponent } from '../component/forms/model/person/person.form.component';
import { DepartmentFormComponent } from '../component/forms/model/department/department.form.component';
import { RoleFormComponent } from '../component/forms/model/role/role.form.component';
import { ContractFormComponent } from '../component/forms/model/contract/contract.form.component';
import { VacationFormComponent } from '../component/forms/absence/vacation/vacation.form.component';
import { TiemsheetFormComponent } from '../component/forms/model/timesheet/timesheet.form.component';

@Injectable({
  providedIn: 'root'
})
export class TopMenuService {

  menuItems: MenuItem[] = [
      new MenuItem('main', 'Հիմնական', null, 'empty', 'Հիմնական մենյու', null, [
        new MenuItem('calculate', 'Հաշվարկել աշխատավարձը', WindowSize.SMALL, 'calculate', 'Տեղեկատու մատյան գրառումներով', GenericTable, []),
        new MenuItem('paylist','Վճարացուցակ', WindowSize.LARGE, 'currency', 'Հաշվարկված աշխատավարձի փոխանցումների ցուցակ', null, [
          new MenuItem('bank-paylist', 'Բանկային', WindowSize.LARGE, 'bank', 'Հաշվարկված աշխատավարձի բանկային փոխանցումների ցուցակ', DepartmentFormComponent, []),
          new MenuItem('chash-paylist', 'Կանխիկ', WindowSize.LARGE, 'cash', 'Հաշվարկված աշխատավարձի կանխիկ փոխանցումների ցուցակ', PersonFormComponent, [])
        ]),
        new MenuItem('payslip', 'Հավարկային թերթիկներ', WindowSize.SMALL, 'payslip', 'Հաշվարկված աշխատավարձի հավարկային թերթիկ', GenericTable, []),
        new MenuItem('period', 'Հաշվետու ամիս', WindowSize.SMALL, 'empty', 'Սահմանել հաշվետու ժամանակարջանը', GenericTable, [])
      ]),
      new MenuItem('action','Գործողություն', null, '', 'Օգտագործվող տեղեկատուների հետ աշխատանք', null, [
        new MenuItem('new','Ավելացնել', null, 'new', 'Տեղեկատուի նոր գրառում', null, [
          new MenuItem('new-department', 'Բաժին', WindowSize.SMALL, 'department', 'Ստեղծել նոր Բաժին', DepartmentFormComponent, []),
          new MenuItem('new-role', 'Պաշտոն', WindowSize.SMALL, 'role', 'Ստեղծել նոր պաշտոն', RoleFormComponent, []),
          new MenuItem('new-person', 'Աշխատակից', WindowSize.MEDIUM, 'person', 'Ստեղծել նոր Անձ', PersonFormComponent, []),
          new MenuItem('new-contract', 'Պայմանագիր', WindowSize.MEDIUM, 'contract', 'Ստեղծել պայմանագիր', ContractFormComponent, [])
        ]),
        new MenuItem('new-absence','Բացակայություն', null, 'empty', 'Գրանցել աշխատողի բացակայություն', null, [
          new MenuItem('new-vacation', 'Հերթական արձակուրդ', WindowSize.MEDIUM, 'vacation', 'Գրանցել հերթական արձակուրդ', VacationFormComponent, []),
          new MenuItem('new-unpaid-vacation', 'Չվճարվող արձակուրդ', WindowSize.LARGE, 'empty', 'Գրանցել իր հաշվին արձակուրդ', DepartmentFormComponent, []),
          new MenuItem('new-SL', 'Անաշխատունակություն', WindowSize.LARGE, 'sl', 'Հիվանդության նպասը', DepartmentFormComponent, []),
          new MenuItem('new-ML', 'Հղիություն/ծննդաբերություն', WindowSize.LARGE, 'ml', 'Հղիության և ծննդաբորության նպաստ', PersonFormComponent, [])
        ]),
        new MenuItem('new-termination','Վերջնահաշվարկ', null, 'empty', 'Աշխատանքից ազատման վերջնահաշվարկ', null, [
          new MenuItem('new-unused-vacation', 'Չօգտագործված արձակուրդ', WindowSize.LARGE, 'empty', 'Չօգտգործված արձակուրդի փոխհատուցման հաշվարկ', DepartmentFormComponent, []),
          new MenuItem('new-fire', 'Արձակման նպաստ', WindowSize.LARGE, 'empty', 'Օրենքով արձակման նպաստի հաշվարկ', PersonFormComponent, [])
        ]),
        new MenuItem('new-timesheet', 'Լրացնել Տաբել', WindowSize.SMALL, 'timesheet', 'Լրացնել աշխատաժամանակի հավարկի ընթացիկ տեղեկագիր', TiemsheetFormComponent, [])
      ]),
      new MenuItem('info','Տեղեկատու', null, '', 'Տեղակետուներ առկա տվյալների', null, [
        new MenuItem('table', 'Մատյան', null, 'lists', 'Տեղեկատու մատյան առկա գրառումներով', null, [
          new MenuItem('table-department', 'Բաժին', WindowSize.RELATIV, 'department', 'Բաժինների տեղեկատու մատյան', GenericTable, []),
          new MenuItem('table-role', 'Պաշտոն', WindowSize.RELATIV, 'role', 'Պատոնների տեղեկատու մատյան', GenericTable, []),
          new MenuItem('table-person', 'Անձ', WindowSize.RELATIV, 'person', 'Անձանց տեղեկատու մատյան', GenericTable, []),
          new MenuItem('table-contract', 'Պայմանագիր', WindowSize.RELATIV, 'contract', 'Պայմանագրերի տեղեկատու մատյան', GenericTable, []),
          new MenuItem('table-timesheet', 'Տաբելներ', WindowSize.LARGE, 'timesheet', 'Լրացված աշխատաժամանակի հավարկի տեղեկագրեր', GenericTable, [])
        ]),
        new MenuItem('orgchart', 'Կառուվածք', WindowSize.LARGE, 'orgchart', 'Աշխատակիցների հիերարխիկ կառուցվածք', GenericTable, []),
        new MenuItem('refresh-', 'Թարմացնել', null, 'reload', 'Թարմացնել տեղեկատուների տվյալները', null, [])
      ]),
      new MenuItem('report', 'Հավետվություններ', null, 'empty', 'Ընթացիկ և նախորդ ժամանակարջանների հաշվետվություններ', null, [
        new MenuItem('management','Կառավարչական', WindowSize.LARGE, 'management', 'Աշխատավարձի մասով կառավարչական հաշվետվություններ', null, [
          new MenuItem('bank-paylist', 'Բանկային', WindowSize.LARGE, 'empty', 'Հաշվարկված աշխատավարձի բանկային փոխանցումների ցուցակ', DepartmentFormComponent, [])
        ]),
        new MenuItem('tax','Հարկային', WindowSize.LARGE, 'tax', 'Աշխատավարձի մասով հարկային հաշվետվություններ', null, [
          new MenuItem('bank-paylist', 'Բանկային', WindowSize.LARGE, 'empty', 'Հաշվարկված աշխատավարձի բանկային փոխանցումների ցուցակ', DepartmentFormComponent, [])
        ]),
        new MenuItem('statistical','Վիճակագրական', WindowSize.LARGE, 'statistical', 'Աշխատավարձի մասով վիճակագրական հաշվետվություններ', null, [
          new MenuItem('1A', '1-Ա ամսեկան', WindowSize.LARGE, 'empty', 'Հաշվարկված աշխատավարձի բանկային փոխանցումների ցուցակ', DepartmentFormComponent, []),
          new MenuItem('avarage', 'Աշխատողների միջին', WindowSize.LARGE, 'avarage', 'Հաշվարկված աշխատավարձի բանկային փոխանցումների ցուցակ', DepartmentFormComponent, [])
        ]),
      ])
  ];

}
