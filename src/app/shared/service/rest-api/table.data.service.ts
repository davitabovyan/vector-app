import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ObjCodeEnum } from '../../enums/objcode.enum';
import { PersonModel } from '../../model/person.model';
import { Logger } from '../logger.service';
import { DepartmentModel } from '../../model/department.model';
import { ContractModel } from '../../model/contract.model';
import { ApiService } from '../api.service';
import { AbsenceModel } from '../../model/absence.model';
import { TimesheetModel } from '../../model/timesheet.model';
import { TableDataParams } from '../../dto/table.data.params';
import { ConverterService } from '../utils/converter.service';

const log = new Logger('TableDataService');

@Injectable({
    providedIn: 'root'
  })
export class TableDataService {

    constructor(
        private http: HttpClient,
        private apiService: ApiService,
        private converterService: ConverterService) {
    }

    public getTypeAheadData(objCode: ObjCodeEnum): any {
        switch(objCode) {
            case ObjCodeEnum.PERSON: 
                return this.getDate<PersonModel>(objCode, new HttpParams().set('fields','code,firstName,lastName,middleName'));
            case ObjCodeEnum.DEPARTMENT: 
                return this.getDate<DepartmentModel>(objCode, new HttpParams().set('fields','code,name'));
            case ObjCodeEnum.ROLE:
                return this.getDate<PersonModel>(objCode, new HttpParams().set('fields','code,name'));
            case ObjCodeEnum.CONTRACT: return this.getDate<ContractModel>(objCode);
            case ObjCodeEnum.EMPLOYMENT: return this.getDate<PersonModel>(objCode);
            case ObjCodeEnum.ABSENCE: return this.getDate<AbsenceModel>(objCode);
            case ObjCodeEnum.PAYSLIP: return this.getDate<PersonModel>(objCode);
            case ObjCodeEnum.TIMESHEET: return this.getDate<TimesheetModel>(objCode);
            case ObjCodeEnum.PERIOD: return sessionStorage.getItem("period");
            default:
        }
    }


    public getTableData(objCode: ObjCodeEnum, params?: TableDataParams): any {
        switch(objCode) {
            case ObjCodeEnum.DEPARTMENT:
                return this.getDate<DepartmentModel>(objCode, new HttpParams().set('fields','*,head:code.firstName.lastName'));
            case ObjCodeEnum.ROLE:
                return this.getDate<PersonModel>(objCode, new HttpParams().set('fields','*,eligibleDays'));
            case ObjCodeEnum.PERSON: return this.getDate<PersonModel>(objCode);
            case ObjCodeEnum.CONTRACT: return this.getDate<ContractModel>(objCode);
            case ObjCodeEnum.EMPLOYMENT: return this.getDate<PersonModel>(objCode);
            case ObjCodeEnum.ABSENCE: return this.getDate<AbsenceModel>(objCode);
            case ObjCodeEnum.PAYSLIP: return this.getDate<PersonModel>(objCode);
            case ObjCodeEnum.TIMESHEET:
                let path = "/byPeriod/" + sessionStorage.getItem("pCurrMonth");
                return this.getDate<TimesheetModel>(objCode, new HttpParams(), path);
            default:
        }
    }

    public getSelectionCriteria(objCode: ObjCodeEnum) : string {
        let criteria = "";
        switch(objCode) {
            case ObjCodeEnum.TIMESHEET:
                let period = this.converterService.parsePeriod(sessionStorage.getItem("pCurrMonth"));
                criteria = ` (${period})`;
            default: 
        }
        return criteria;
    }
  
    private getDate<T>(objCode: ObjCodeEnum, params?: HttpParams, path?: string): Observable<T[]> {
        if(!params) params = new HttpParams().set('fields','*');
        let url = `${this.apiService.API_PREFIX}:8444/api/v1/${objCode.toLowerCase()}`;
        if(path) url = url + path;
        return this.http
            .get<T[]>(url,{params})
            .pipe(
                catchError(err => {
                    log.debug(`Fetch ${objCode} list failed!`);
                    return throwError(err);
                })
            );
    }

} 