import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ApiService } from '../api.service';

@Injectable({
    providedIn: 'root'
  })
export class AuthService {

    constructor(private http: HttpClient, private apiService: ApiService) {

    }

    login(email:string, tin:string, password:string ) {
        return this.http.post<{token:string,expires:string}>(this.apiService.API_PREFIX + ':8443/login?username=' + email + '<' + tin + '>&password=' + password, {})
            .subscribe(res => this.setSession(res));
    }
          
    private setSession(authResult: any) {
        localStorage.setItem('auth_token', authResult.token);
        localStorage.setItem("expires_at", authResult.expires);
        window.location.reload();
    }          

    logout() {
        localStorage.removeItem("auth_token");
        localStorage.removeItem("expires_at");
        window.location.reload();
    }

    public isLoggedIn() {
        return localStorage.getItem("expires_at") && 
        Number(localStorage.getItem("expires_at")) > Date.now();
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    } 
}
