import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from 'src/app/shared/service/logger.service';
import { FormGroup } from '@angular/forms';
import { ControllerService } from '../controller.service';
import { RoleModel } from 'src/app/shared/model/role.model';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { ApiService } from '../../api.service';

const log = new Logger('RoleContrllerService');

@Injectable({
  providedIn: 'root'
})
export class RoleContrllerService extends ControllerService {

  private readonly objCode = ObjCodeEnum.ROLE;
  private readonly API_PATH = this.API_PREFIX + this.objCode + "/";

  constructor(private http: HttpClient, public apiService: ApiService) {
    super(http, apiService);
  }

  public update(role: RoleModel, form: FormGroup): Observable<any> {
    return this.http
      .put(this.API_PATH + role.uuid, this.convertToModel(role, form.value))          
      .pipe(
        catchError(err => {
          log.debug(`Role update failed!`);
          return throwError(err);
        })
      );
  }

  public saveRole(role: RoleModel, form: FormGroup): Observable<any> {
    return this.save(this.convertToModel(role, form.value), this.objCode);
  }

  public convertToModel(model: RoleModel, form: FormGroup): any {
    Object.assign(model, form);
    return model;
  }
}