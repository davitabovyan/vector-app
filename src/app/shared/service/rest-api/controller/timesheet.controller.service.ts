import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from 'src/app/shared/service/logger.service';
import { FormGroup } from '@angular/forms';
import { ControllerService } from '../controller.service';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { ApiService } from '../../api.service';
import { TimesheetModel } from 'src/app/shared/model/timesheet.model';

const log = new Logger('TimesheetContrllerService');

@Injectable({
  providedIn: 'root'
})
export class TimesheetContrllerService extends ControllerService {

  private readonly objCode = ObjCodeEnum.TIMESHEET;
  private readonly API_PATH = `${this.API_PREFIX}:8444/api/v1/${this.objCode}`;

  constructor(private http: HttpClient, public apiService: ApiService) {
    super(http, apiService);
  }

  public saveTimesheet(timesheet: TimesheetModel, form: FormGroup): Observable<any> {
    return this.save(this.convertToModel(timesheet, form.value), this.objCode);
  }

  public calculateTimesheet(period: string, department: string | null): Observable<any> {
    return this.http
      .post(`${this.API_PATH}/calculate/${period}`,{})
      .pipe(
        catchError(err => {
          log.debug(`Timesheet update failed!`);
          return throwError(err);
        })
      );
  }

  public convertToModel(model: TimesheetModel, form: FormGroup): any {
    Object.assign(model, form);
    return model;
  }
}