import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from 'src/app/shared/service/logger.service';
import { FormGroup } from '@angular/forms';
import { ControllerService } from '../controller.service';
import { DepartmentModel } from 'src/app/shared/model/department.model';
import { PersonModel } from 'src/app/shared/model/person.model';
import { ContractModel } from 'src/app/shared/model/contract.model';
import { RoleModel } from 'src/app/shared/model/role.model';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import moment from 'moment';
import { ApiService } from '../../api.service';

const log = new Logger('ContractContrllerService');

@Injectable({
  providedIn: 'root'
})
export class ContractContrllerService extends ControllerService {

  private readonly objCode = ObjCodeEnum.CONTRACT;
  private readonly API_PATH = this.API_PREFIX + ":8444/api/v1/" + this.objCode + "/";

  constructor(private http: HttpClient, public apiService: ApiService) {
    super(http, apiService);

  }

  public update(contract: ContractModel, form: FormGroup,
     person: PersonModel, role: RoleModel, department: DepartmentModel): Observable<any> {
  
    return this.http
      .put(this.API_PATH+ contract.uuid, this.convertToModel(contract, form.value, person, role, department))          
      .pipe(
        catchError(err => {
          log.debug(`${this.objCode} update failed!`);
          return throwError(err);
        })
      );
  }

  public saveContract(contract: ContractModel, form: FormGroup,
    person: PersonModel, role: RoleModel, department: DepartmentModel): Observable<any> {

    return this.save(this.convertToModel(contract, form.value, person, role, department), this.objCode);
  }

  public convertToModel(model: ContractModel, form: FormGroup, 
    person: PersonModel, role: RoleModel, department: DepartmentModel): any {

    Object.assign(model, form);
    model.person = person;
    model.role = role;
    model.department = department;
    model.salary *= 100;
    model.startDate = moment(model.startDate).format("YYYY-M-D");
    if(model.endDate) model.endDate = moment(model.endDate).format("YYYY-M-D");
    return model;
  }
}