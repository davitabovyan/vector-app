import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from 'src/app/shared/service/logger.service';
import { FormGroup } from '@angular/forms';
import { ControllerService } from '../controller.service';
import { DepartmentModel } from 'src/app/shared/model/department.model';
import { PersonModel } from 'src/app/shared/model/person.model';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { ApiService } from '../../api.service';

const log = new Logger('DepartmentContrllerService');

@Injectable({
  providedIn: 'root'
})
export class DepartmentContrllerService extends ControllerService {

  private readonly objCode = ObjCodeEnum.DEPARTMENT;
  private readonly API_PATH = this.API_PREFIX + this.objCode + "/";

  constructor(private http: HttpClient, public apiService: ApiService) {
    super(http, apiService);
  }

  public update(department: DepartmentModel, form: FormGroup, head: PersonModel): Observable<any> {
    return this.http
      .put(this.API_PATH + department.uuid, this.convertToModel(department, form.value, head))          
      .pipe(
        catchError(err => {
          log.debug(`${this.objCode} update failed!`);
          return throwError(err);
        })
      );
  }

  public saveDepartment(department: DepartmentModel, form: FormGroup, head: PersonModel): Observable<any> {
    return this.save(this.convertToModel(department, form.value, head), this.objCode);
  }

  public convertToModel(model: DepartmentModel, form: FormGroup, head: PersonModel): any {
    Object.assign(model, form);
    model.head = head;
    return model;
  }
}