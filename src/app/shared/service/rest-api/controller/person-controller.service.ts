import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PersonModel } from 'src/app/shared/model/person.model';
import { Logger } from 'src/app/shared/service/logger.service';
import { FormGroup } from '@angular/forms';
import { ControllerService } from '../controller.service';
import moment from 'moment';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { ApiService } from '../../api.service';

const log = new Logger('PersonContrllerService');

@Injectable({
  providedIn: 'root'
})
export class PersonContrllerService extends ControllerService {

  private readonly objCode = ObjCodeEnum.PERSON;
  private readonly API_PATH = this.API_PREFIX + this.objCode + "/";

  constructor(private http: HttpClient, public apiService: ApiService) {
    super(http, apiService);
  }

  public update(person: PersonModel, form: FormGroup): Observable<any> {
    return this.http
      .put(this.API_PATH + person.uuid, this.convertToModel(person, form.value))          
      .pipe(
        catchError(err => {
          log.debug(`Person update failed!`);
          return throwError(err);
        })
      );
  }

  public savePerson(person: PersonModel, form: FormGroup): Observable<any> {
    return this.save(this.convertToModel(person, form.value), this.objCode);
  }

  public convertToModel(model: PersonModel, form: FormGroup): any {
    Object.assign(model, form)
    model.birthday = moment(model.birthday).format("YYYY-M-D");
    return model;
  }
}