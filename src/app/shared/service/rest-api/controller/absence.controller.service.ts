import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from 'src/app/shared/service/logger.service';
import { FormGroup } from '@angular/forms';
import { ControllerService } from '../controller.service';
import { PersonModel } from 'src/app/shared/model/person.model';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { ApiService } from '../../api.service';
import { AbsenceModel } from 'src/app/shared/model/absence.model';
import moment from 'moment';

const log = new Logger('DepartmentContrllerService');

@Injectable({
  providedIn: 'root'
})
export class AbsenceContrllerService extends ControllerService {

  private readonly objCode = ObjCodeEnum.ABSENCE;
  private readonly API_PATH = this.API_PREFIX + this.objCode + "/";

  constructor(private http: HttpClient, public apiService: ApiService) {
    super(http, apiService);
  }

  public update(absence: AbsenceModel, form: FormGroup, person: PersonModel): Observable<any> {
    return this.http
      .put(this.API_PATH + absence.uuid, this.convertToModel(absence, form.value, person))          
      .pipe(
        catchError(err => {
          log.debug(`${this.objCode} update failed!`);
          return throwError(err);
        })
      );
  }

  public saveAbsence(absence: AbsenceModel, form: FormGroup, person: PersonModel): Observable<any> {
    return this.save(this.convertToModel(absence, form.value, person), this.objCode);
  }

  public convertToModel(model: AbsenceModel, form: FormGroup, person: PersonModel): any {
    Object.assign(model, form);
    model.person = person;
    model.startDate = moment(model.startDate).format("YYYY-M-D");
    model.endDate = moment(model.endDate).format("YYYY-M-D");
    return model;
  }
}