import { Injectable } from '@angular/core';
import { ObjCodeEnum } from '../../enums/objcode.enum';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Logger } from '../logger.service';
import { ApiService } from '../api.service';

const log = new Logger('ControllerService');

@Injectable({
    providedIn: 'root'
})
export class ControllerService {

    private httpClient: HttpClient;
    public API_PREFIX: string;

    constructor(http: HttpClient, public apiService: ApiService) {
        this.httpClient = http;
        this.API_PREFIX = apiService.API_PREFIX;
    }

    public save(data: any, objCode: ObjCodeEnum): Observable<any> {
        return this.httpClient
          .post(this.API_PREFIX + ':8444/api/v1/' + objCode, data)          
          .pipe(
            catchError(err => {
              log.debug(`${objCode} creation failed!`);
              return throwError(err);
            })
          );
    }

    public remove(uuid: string, objCode: ObjCodeEnum): Observable<any> {
        return this.httpClient
          .delete(this.API_PREFIX + ':8444/api/v1/' + objCode + "/" + uuid)          
          .pipe(
            catchError(err => {
              log.debug(`Failed to remove ${objCode} with uuid: ${uuid}`);
              return throwError(err);
            })
          );
    }
}