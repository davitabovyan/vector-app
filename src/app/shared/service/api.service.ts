import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    // public readonly API_PREFIX = 'https://api-vector.com';
    public readonly API_PREFIX = 'https://localhost';
}