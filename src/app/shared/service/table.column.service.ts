import { Injectable } from '@angular/core';
import { ValueTypeEnum } from '../enums/value.type';

@Injectable({
  providedIn: 'root'
})
export class TableColumnService {
  tableColumns = {
    person: [
      {key:'code', value: 'Կոդ', width: 30, default: true, type: ValueTypeEnum.STRING},
      {key:'ssn', value: 'ՀԾՀ', width: 30, default: true, type: ValueTypeEnum.STRING},
      {key:'firstName', value: 'Անուն', width: 60, default: true, type: ValueTypeEnum.STRING},
      {key:'middleName', value: 'Հայրանուն', width: 60, default: true, type: ValueTypeEnum.STRING},
      {key:'lastName', value: 'Ազգանուն', width: 70, default: true, type: ValueTypeEnum.STRING},
      {key:'cell', value: 'Բջջային', width: 50, default: false, type: ValueTypeEnum.STRING},
      {key:'email', value: 'Էլ․հասցե', width: 50, default: false, type: ValueTypeEnum.STRING}
    ],

    role: [
      {key:'code', value: 'Կոդ', width: 30, default: true, type: ValueTypeEnum.STRING},
      {key:'name', value: 'Անվանում', width: 150, default: true, type: ValueTypeEnum.STRING},
      {key:'description', value: 'Նկարագրություն', width: 200, default: true, type: ValueTypeEnum.STRING},
      {key:'eligibleDays', value: 'Հասանելի օրեր', width: 10, default: false, type: ValueTypeEnum.NUMBER}
    ],

    department: [
      {key:'code', value: 'Կոդ', width: 30, default: true, type: ValueTypeEnum.STRING},
      {key:'name', value: 'Անվանում', width: 150, default: true, type: ValueTypeEnum.STRING},
      {key:'costCenter', value: 'Ծախս. կենտր․', width: 40, default: false, type: ValueTypeEnum.STRING},
      {key:'head', value: 'Բաժնի ղեկավարը', width: 200, default: true, type: ValueTypeEnum.STRING}
    ],

    contract: [
      {key:'department', value: 'Բաժին', width: 100, default: true, type: ValueTypeEnum.STRING},
      {key:'person', value: 'Աշխատող', width: 200, default: true, type: ValueTypeEnum.STRING},
      {key:'role', value: 'Պաշտոն', width: 80, default: true, type: ValueTypeEnum.STRING},
      {key:'salary', value: 'Աշխատավարձ', width: 30, default: true, type: ValueTypeEnum.NUMBER},
      {key:'startDate', value: 'Սկիզբ', width: 70, default: true, type: ValueTypeEnum.DATE},
      {key:'endDate', value: 'Ավարտ', width: 70, default: true, type: ValueTypeEnum.DATE},
      {key:'dailyHours', value: 'Օրական աշխ․ժամ', width: 10, default: false, type: ValueTypeEnum.NUMBER},
      {key:'laborRelation', value: 'Պայմանագրի տիպը', width: 40, default: true, type: ValueTypeEnum.TAGGLE},
      {key:'workWeek', value: 'Աշխատանքային շաբաթ', width: 10, default: false, type: ValueTypeEnum.NUMBER}
    ],

    employment: new Map()
      .set('hireDate','')
      .set('terminationDate','')
      .set('vacationBalance',''),

    absence: new Map()
      .set('leaveType','')
      .set('amount','')
      .set('period','')
      .set('startDate','')
      .set('endDate','')
      .set('isParent','')
      .set('parentId','')
      .set('person',''),

    payslip: new Map()
      .set('period','')
      .set('wage','')
      .set('overtime','')
      .set('bonus','')
      .set('ssp','')
      .set('it','')
      .set('army','')
      .set('hasUpdate','')
      .set('person',''),

    timesheet: [
      {key:'code', value: 'Կոդ', width: 60, default: true, type: ValueTypeEnum.STRING, sortable: true},
      {key:'departmentCode', value: 'Բաժին', width: 50, default: true, type: ValueTypeEnum.STRING, sortable: true},
      {key:'name', value: 'Աշխատող', width: 120, default: true, type: ValueTypeEnum.STRING, sortable: true},
      {key:'roleName', value: 'Պաշտոն', width: 80, default: true, type: ValueTypeEnum.STRING, sortable: true},
      {key:'total', value: 'Ընդ․', width: 40, default: true, type: ValueTypeEnum.DAY, sortable: true},
    ]
  }

  generalTypeAheadMap = new Map()
  .set('code','Կոդ')
  .set('name','Անվանում');

  typeAheadColumns = {
    person: new Map()
    .set('code', 'Կոդ')
    .set('lastName', 'Ազգանուն')
    .set('firstName', 'Անուն')
    .set('middleName', 'Հայրանուն'),

    role: this.generalTypeAheadMap,
    department: this.generalTypeAheadMap,
    period: this.generalTypeAheadMap
  }

  public getField(object: string, field: string) : any {
    let fields = this.tableColumns[object];
    for(let i=0;i<fields.length;i++) {
      if(fields[i].key == field) return fields[i];
    }
    throw new Error(`There is no ${field} field in ${object} object`);
  }

  public getRulesByFieldName(object: string, field: string) : string {
    return this.getRulesByType(this.getField(object, field).type);
  }

  public getRulesByType(type: ValueTypeEnum) : string {
    switch(type) {
      case ValueTypeEnum.NUMBER:
        return "Միայն ամբողջ թիվ կամ առավելագույն 2 թիվ \"․\"-ից հետո";
      default:
        return "";
    }
  }

}
