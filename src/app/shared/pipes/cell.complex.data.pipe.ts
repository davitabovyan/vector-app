import { Pipe, PipeTransform } from '@angular/core';

import { ValueTypeEnum } from '../enums/value.type';
import { ConverterService } from '../service/utils/converter.service';


@Pipe({
  name: 'cellComplexData'
})
export class CellComplexDataPipe implements PipeTransform {

  constructor(private converterService: ConverterService) {
  }

  transform(row: any, type: ValueTypeEnum): string {
    if (typeof row === 'object' && row !== null) {
        let str = '';
        for (let [key, value] of Object.entries(row)) {
          if (key === 'uuid' || key === 'code') {
            continue;
          } else {
            str += value;
            str += ' ';
          }
        }
        return str;
    } else {
      switch(type){
        case ValueTypeEnum.NUMBER:
          return this.converterService.formatNumber(row);
        case ValueTypeEnum.DAY:
          return this.converterService.formatDay(row);
        default:
          return row;
      }
    }
  }
}
