import { Pipe, PipeTransform } from '@angular/core';
import reduce from 'lodash/reduce';
import filter from 'lodash/filter';

import { ValueTypeEnum } from '../enums/value.type';
import { ConverterService } from '../service/utils/converter.service';


@Pipe({
  name: 'tableFooterData'
})
export class TableFooterDataPipe implements PipeTransform {

  constructor(private converterService: ConverterService) {
  }

  transform(column:string, data: any[], type: ValueTypeEnum, first: boolean, selected: Set<any>): string {
    if(data == undefined) return '';
    if(first) return data.length + ' տող';
  
    switch(type){
      case ValueTypeEnum.NUMBER:
        if(selected.size > 0) {
          data = filter(data,function(o) { return selected.has(o.uuid)});
        }
        let sum = reduce(data, function(sum, n) {return sum + n[column]}, 0);
        return this.converterService.formatNumber(sum);
      default:
        return '';
    }
  }
}
