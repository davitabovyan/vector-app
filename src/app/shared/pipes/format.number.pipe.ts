import { Pipe, PipeTransform } from '@angular/core';
import { ConverterService } from '../service/utils/converter.service';

@Pipe({
    name: 'formatNumber'
  })
  export class FormatNumberPipe implements PipeTransform {
  
    constructor(private converterService: ConverterService) {
    }
  
    transform(num:number): string {
        return num != 0 ? this.converterService.formatNumber(num) : '';
    }
  }
  