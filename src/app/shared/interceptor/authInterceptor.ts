import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthService } from "../service/rest-api/auth.service";

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken = localStorage.getItem("auth_token");
        const authRequest = request.clone({
            headers: request.headers
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${authToken}`)
        });
        return next.handle(authRequest);
    }
}