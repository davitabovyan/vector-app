export enum ObjCodeEnum {
    PERSON = 'person',
    ROLE = 'role',
    DEPARTMENT = 'department',
    EMPLOYMENT = 'employment',
    CONTRACT = 'contract',
    ABSENCE = 'absence',
    PAYSLIP = 'payslip',
    TIMESHEET = 'timesheet',
    PERIOD = 'period'
}