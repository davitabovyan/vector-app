import { Injectable } from '@angular/core';
import { Pair } from '../model/system/pair';

@Injectable({
    providedIn: 'root'
  })
export class EnumService {
    private genders: Pair[] = [new Pair("MALE", "Տղամարդ"), new Pair("FEMALE", "Կին")];

    public getGenders() : Pair[] {
        return this.genders;
    }

    public getValues(list: Pair[]): string[] {
        let values: string[] = [];
        for(let pair of list) {
            values.push(pair.key);
        }
        return values;
    }

    public getValueByKey(list: Pair[], key: string): string {
        for (let i in list) {
            if (list[i].key == key) {
                return list[i].value;
            }
        }
        throw new Error(`No key ${key} in the list`);
    }
}