export enum AbsenceType {
    VACATION,
    SICK_LEAVE,
    MATERNITY_LEAVE,
    UNPAID_LEAVE
}