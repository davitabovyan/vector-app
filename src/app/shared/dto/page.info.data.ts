export class PageInfoData {
    constructor(
        public page: string,
        public data: any
    ) {}
}