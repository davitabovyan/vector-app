import { GenericWindow } from './generic.window';
import { WindowData } from './window.data';
import { EventEmitter } from '@angular/core';


export interface FormDataInterface extends GenericWindow {
    setData(data: any);
    setWindowData(windowData: WindowData);
    getCloseWindowId() : EventEmitter<number>;
}