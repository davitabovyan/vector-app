import { Type } from '@angular/core';
import { WindowSize } from '../enums/window.size';
import { WindowData } from './window.data';
import { FormDataInterface } from './form.data.interface';

export class FormWindowData extends WindowData {
  constructor(
      public component: Type<FormDataInterface>,
      size: WindowSize | null,
      title: string,
      index: number,
      id: number,
      page: string
    ) {
        super(size, title, index, id, page);
    }
}
