import { WindowSize } from '../enums/window.size';

export class WindowData {
  constructor(
      public size: WindowSize | null,
      public title: string,
      public index: number,
      public id: number,
      public page: string
    ) {}
}
