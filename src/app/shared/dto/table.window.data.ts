import { Type } from '@angular/core';
import { WindowSize } from '../enums/window.size';
import { GenericTable } from '../component/tables/generic/generic-table';
import { WindowData } from './window.data';

export class TableWindowData extends WindowData {
  constructor(
      public component: Type<GenericTable>,
      size: WindowSize | null,
      title: string,
      index: number,
      id: number,
      page: string = ''
    ) {
        super(size, title, index, id, page);
    }
}
