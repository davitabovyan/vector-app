import { HttpParams } from "@angular/common/http";

export class TableDataParams {
  constructor(
      public params: HttpParams,
      public path: string = ''
    ) {}
}
