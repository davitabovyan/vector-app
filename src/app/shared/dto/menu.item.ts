import { Type } from '@angular/core';
import { WindowSize } from '../enums/window.size'
import { GenericWindow } from './generic.window';

export class MenuItem {
    constructor(
        public key: string,
        public arm: string,
        public size: WindowSize | null,
        public icon: string,
        public tooltip: string,
        public type: Type<GenericWindow> | null,
        public inner: MenuItem[]
      ) {}
}