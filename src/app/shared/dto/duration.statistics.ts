export class DurationStatistics {
    constructor(
        public total: number = 0,
        public working: number = 0,
        public suturday: number = 0,
        public sunday: number = 0,
        public holiday: number = 0,
        public holidayNames: string[] = []
      ) {}
  }
  