import { BaseModel } from './base.model';
import { PersonModel } from './person.model';
import { RoleModel } from './role.model';
import { DepartmentModel } from './department.model';

export interface ContractModel extends BaseModel {
  salary: number;
  startDate: string;
  endDate: string;
  dailyHours: number;
  laborRelation: boolean;
  workWeek: number;
  person: PersonModel;
  role: RoleModel;
  department: DepartmentModel;
}