import { BaseModel } from './base.model';

export interface RoleModel extends BaseModel {
  code: string;
  name: string;
  description: string;
  eligibleDays: number;
}