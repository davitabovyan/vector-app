import { AbsenceType } from '../enums/absence.type';
import { BaseModel } from './base.model';
import { PersonModel } from './person.model';

export interface AbsenceModel extends BaseModel {
    leaveType: AbsenceType;
    amount: number;
    period: string;
    startDate: string;
    endDate: string;
    isParent: boolean;
    parentId: string;
    person: PersonModel;
}