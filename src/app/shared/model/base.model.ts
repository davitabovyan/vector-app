export interface BaseModel {
    uuid: string;
    state: string;
    tin: string;
    recorderUuid: string;
  }