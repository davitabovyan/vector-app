
export interface TypeaheadModel {
    code: string;
    name: string;
}