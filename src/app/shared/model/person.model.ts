import { BaseModel } from './base.model';

export interface PersonModel extends BaseModel {
  ssn: string;
  code: string;
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  cell: string;
  birthday: string;
  gender: string;
}