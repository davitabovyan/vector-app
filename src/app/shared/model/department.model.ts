import { BaseModel } from './base.model';
import { PersonModel } from './person.model';

export interface DepartmentModel extends BaseModel {
  code: string;
  name: string;
  costCenter: string;
  head: PersonModel;
}