import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../service/rest-api/auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  form: FormGroup;
  private readonly emailPattern = /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
    ) {}

    ngOnInit() {
      this.form = this.formBuilder.group({
        user: this.formBuilder.control('user', Validators.compose([
          Validators.required
          // Validators.pattern(this.emailPattern)
        ])),
        tin: this.formBuilder.control('tin', Validators.compose([
          Validators.required,
          Validators.pattern(/^\d{8}$/)
        ])),
        pass: this.formBuilder.control('pass', Validators.required)
      });
    }

  public login({user, tin, pass}): void {
    // const {user, pass} = this.form.value;
    this.authService.login(user, tin, pass);

  }

}
