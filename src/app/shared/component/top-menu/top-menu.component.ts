import {Component, EventEmitter, OnInit, Output } from '@angular/core';
import {TopMenuService} from "../../service/top-manue.service";
import {AuthService} from "../../service/rest-api/auth.service";
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import { IconService } from '../../service/component/icon.service';


/** Custom options the configure the tooltip's default show/hide delays. */
export const customTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 0,
  touchendHideDelay: 0,
};

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css'],
  providers: [
    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: customTooltipDefaults}
  ],
})
export class TopMenuComponent implements OnInit {


  menuData: object[];
  position = ['right','above'];

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();
  
  constructor(
    public topMenuService: TopMenuService,
    private authService: AuthService,
    private iconService: IconService) {
    
    this.menuData = this.topMenuService.menuItems;
    iconService.register();
   }

  ngOnInit() {
  }

  openTable(page: string): void {
    this.notify.emit(page);  
  }

  signOut(): void {
    this.authService.logout();
  }

}
