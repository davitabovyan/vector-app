import {Component, Input, OnInit} from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {

  @Input() page: string[];
  constructor() { }

  ngOnInit() {
  }

  closeAllWindows(): void {
    _.pullAll(this.page, this.page);
  }

}
