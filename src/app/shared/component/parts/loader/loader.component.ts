import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: "app-loader",
    templateUrl: "./loader.component.html"
})
export class LoaderComponent {
    constructor(
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer) {
    
          iconRegistry.addSvgIcon('loading', sanitizer.bypassSecurityTrustResourceUrl(`assets/svg/icon/tail-spin.svg`));
      }
}