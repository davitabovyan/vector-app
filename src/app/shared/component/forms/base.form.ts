import { ElementRef } from '@angular/core';
import { WindowControllService } from '../../service/component/window.controll.service';
import { FormGroup } from '@angular/forms';
import { TableColumnService } from '../../service/table.column.service';


export class BaseForm {

    constructor(
        public elementRef: ElementRef,
        public windowControllService: WindowControllService,
        public tableColumnService: TableColumnService) {
        
    }

    openTypeAheadBase(objCode: string, windowId: number) {
        if(this.windowControllService.isCurrentActiveWindow(windowId)) {
            this.windowControllService.typeAhead = objCode;
            this.elementRef.nativeElement.querySelector('#modal-overlay-' + objCode).classList.toggle("open");
            let typeahead = this.elementRef.nativeElement.querySelector('#typeahead-' + objCode);
            typeahead.classList.toggle("open");
        }
    }

    showErrorsBase(form:FormGroup, windowId: number) {
        let errors = '';
        let fields = Object.keys(form.controls);
        for(let i=0;i<fields.length;i++){
          if(form.controls[fields[i]].invalid){
            errors += this.tableColumnService.getField('contract', fields[i]).value + ' - ' + form.controls[fields[i]].value +
                     ' (' + this.tableColumnService.getRulesByFieldName('contract', fields[i]) + ')';
          }
        }
        this.elementRef.nativeElement.querySelector('#err_' + windowId).innerHTML = errors;
    }

}