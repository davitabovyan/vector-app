import {Component, Output, EventEmitter, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';

import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { WorkAreaComponent } from '../../../work-area/work-area.component';
import { PersonModel } from 'src/app/shared/model/person.model';
import { FormService } from 'src/app/shared/service/component/form.service';
import { IconService } from 'src/app/shared/service/component/icon.service';
import { AbsenceModel } from 'src/app/shared/model/absence.model';
import { AbsenceContrllerService } from 'src/app/shared/service/rest-api/controller/absence.controller.service';
import { DateService } from 'src/app/shared/service/utils/date.service';
import { DurationStatistics } from 'src/app/shared/dto/duration.statistics';

const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-vacation-form',
    templateUrl: './vacation.form.component.html',
    styleUrls: ['./vacation.form.component.less'],
  })
export class VacationFormComponent implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public isLoading = false;
  public breakdown = new DurationStatistics();
  public absence: AbsenceModel;
  
  public person: PersonModel;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();
  
  @ViewChild('typeahead') typeahead: WorkAreaComponent;
  
  form: FormGroup;

  constructor(
    private windowControllService: WindowControllService,
    private absenceControllerService: AbsenceContrllerService,
    private formService: FormService,
    private dateService: DateService,
    private iconService: IconService,
    private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.form = new FormGroup({
        person: new FormControl(this.absence.person.code || ''),
        start: new FormControl(moment.utc(this.absence.startDate || moment(), "YYYY-M-D")),
        end: new FormControl(moment.utc(this.absence.endDate || '', "YYYY-M-D")),
        duration: new FormControl('')
    });

    this.form.get('duration')?.valueChanges.subscribe(val => {
        this.form.patchValue({
            end: this.dateService.calculateEndDate(this.form.get('start')?.value, val, true)
        })
    });
    this.form.get('end')?.valueChanges.subscribe(val => {
        if(this.form.get('duration')?.value == ""){
            this.breakdown = this.dateService.calculateDuration(this.form.get('start')?.value, val, true);
            this.form.patchValue({
                duration: this.breakdown.working
            },{ emitEvent: false })
        }
    });
  }

  setWindowData(windowData: WindowData) {
    this.windowData = windowData;
  }

  getCloseWindowId() : EventEmitter<number> {
    return this.closeWindowId;
  }

  setData(data: any) {
    if (!data) {
      this.initializeData();
    } else {
      this.absence = data;
    }
    this.person = this.absence.person;
  }

  selectTypeAhead(person: PersonModel){
    this.form.controls.person.setValue(person.code);
    this.person = person;
  }

  public closeOnEscape(id: number) {
    if(this.form.untouched) {
      this.closeWindow(id);
    }
  }

  public closeWindow(id: number) {
    this.closeWindowId.emit(id);
  }

  public updateDuration() {
    this.breakdown = this.dateService.calculateDuration(
        this.form.get('start')?.value,
        this.form.get('end')?.value,
        true
    );
    this.form.patchValue({
        duration: this.breakdown.working
    },{ emitEvent: false })
  }

  public tabChanged($event) {
      if($event.index == 1) {
        this.breakdown = this.dateService.calculateDuration(
            this.form.get('start')?.value,
            this.form.get('end')?.value,
            true
        );
      }
  }

  public save() {
    if(!this.form.valid) return;
    if(this.absence.uuid) {
      this.absenceControllerService.update(this.absence, this.form, this.person)
        .subscribe(r=>{
          this.closeWindow(this.windowData.id);
        });
    } else {
      this.absenceControllerService.saveAbsence(this.absence, this.form, this.person)
      .subscribe(r=>{
        this.absence.uuid = r.uuid;
        this.closeWindow(this.windowData.id);
      });
    }
    this.form.disable();
    this.isLoading = true;
  }

  focus() {
    this.windowControllService.focusWindow(this.windowData.id);
  }

  openTypeAhead(event: MouseEvent, objCode: string) {
    this.elementRef.nativeElement.querySelector('#modal-overlay-' + objCode).classList.toggle("open");
    let typeahead = this.elementRef.nativeElement.querySelector('#typeahead-' + objCode);
    typeahead.setAttribute("top",event.clientX + "px");
    typeahead.setAttribute("left",event.clientY + "px");
    typeahead.classList.toggle("open");
  }

  private initializeData() {
    if (!this.absence) {
      this.absence = {} as AbsenceModel;
    }
    if (!this.absence.person) {
      this.absence.person = {} as PersonModel;
    }
  }
}