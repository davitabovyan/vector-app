import {Component, Output, EventEmitter, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { DepartmentModel } from 'src/app/shared/model/department.model';
import { DepartmentContrllerService } from 'src/app/shared/service/rest-api/controller/department.controller.service';
import { WorkAreaComponent } from '../../../work-area/work-area.component';
import { PersonModel } from 'src/app/shared/model/person.model';
import { FormService } from 'src/app/shared/service/component/form.service';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { IconService } from 'src/app/shared/service/component/icon.service';

@Component({
    selector: 'app-department-form',
    templateUrl: './department.form.component.html',
    styleUrls: ['./department.form.component.less'],
  })
export class DepartmentFormComponent implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public isLoading = false;
  public department: DepartmentModel;
  
  public head: PersonModel;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();
  
  @ViewChild('typeahead') typeahead: WorkAreaComponent;
  
  form: FormGroup;

  constructor(
    private windowControllService: WindowControllService,
    private departmentController: DepartmentContrllerService,
    private formService: FormService,
    private iconService: IconService,
    private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.initializeData();
    this.form = new FormGroup({
      'code': new FormControl(this.department.code || '', [Validators.required, Validators.pattern(/^\d{2,6}$/)]),
      'name': new FormControl(this.department.name || '', Validators.required),
      'costCenter': new FormControl(this.department.costCenter || ''),
      'head': new FormControl(this.department.head.code || '')
    });
  }

  setWindowData(windowData: WindowData) {
    this.windowData = windowData;
  }

  getCloseWindowId() : EventEmitter<number> {
    return this.closeWindowId;
  }

  setData(data: any) {
    if (!data) {
      this.initializeData();
    } else {
      this.department = data;
    }
    this.head = this.department.head;
  }

  selectTypeAhead(person: PersonModel){
    this.form.controls.head.setValue(person.code);
    this.head = person;
  }

  public closeOnEscape(id: number) {
    if(this.form.untouched) {
      this.closeWindow(id);
    }
  }

  public closeWindow(id: number) {
    this.closeWindowId.emit(id);
  }

  public save() {
    if(!this.form.valid) return;
    if(this.department.uuid) {
      this.departmentController.update(this.department, this.form, this.head)
        .subscribe(r=>{
          this.closeWindow(this.windowData.id);
        });
    } else {
      this.departmentController.saveDepartment(this.department, this.form, this.head)
      .subscribe(r=>{
        this.department.uuid = r.uuid;
        this.closeWindow(this.windowData.id);
      });
    }
    this.form.disable();
    this.isLoading = true;
  }

  focus() {
    this.windowControllService.focusWindow(this.windowData.id);
  }

  openTypeAhead(event: MouseEvent, objCode: string) {
    this.elementRef.nativeElement.querySelector('#modal-overlay-' + objCode).classList.toggle("open");
    let typeahead = this.elementRef.nativeElement.querySelector('#typeahead-' + objCode);
    typeahead.setAttribute("top",event.clientX + "px");
    typeahead.setAttribute("left",event.clientY + "px");
    typeahead.classList.toggle("open");
  }

  private initializeData() {
    if (!this.department) {
      this.department = {} as DepartmentModel;
    }
    if (!this.department.code) {
      this.department.code = this.formService.getNextCode(ObjCodeEnum.DEPARTMENT);
    }
    if (!this.department.head) {
      this.department.head = {} as PersonModel;
    }
  }
}