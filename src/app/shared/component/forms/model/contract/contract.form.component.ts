import {Component, Output, EventEmitter, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';

import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { DepartmentModel } from 'src/app/shared/model/department.model';
import { WorkAreaComponent } from '../../../work-area/work-area.component';
import { PersonModel } from 'src/app/shared/model/person.model';
import { ContractModel } from 'src/app/shared/model/contract.model';
import { RoleModel } from 'src/app/shared/model/role.model';
import { ContractContrllerService } from 'src/app/shared/service/rest-api/controller/contract.controller.service';
import { IconService } from 'src/app/shared/service/component/icon.service';
import { TableColumnService } from 'src/app/shared/service/table.column.service';
import { BaseForm } from '../../base.form';

const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-contract-form',
    templateUrl: './contract.form.component.html',
    styleUrls: ['./contract.form.component.less'],
  })
export class ContractFormComponent extends BaseForm implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public isLoading = false;
  public contract: ContractModel;
  
  public department: DepartmentModel;
  public person: PersonModel;
  public role: RoleModel;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();
  
  @ViewChild('typeahead') typeahead: WorkAreaComponent;
  
  form: FormGroup;

  constructor(
    public elementRef: ElementRef, 
    public windowControllService: WindowControllService,
    public tableColumnService: TableColumnService,
    private contractController: ContractContrllerService,
    private iconService: IconService) {
      super(elementRef, windowControllService, tableColumnService);
      iconService.register();
  }

  ngOnInit() {
    this.initializeData();
    this.form = new FormGroup({
      'salary': new FormControl(this.contract.salary / 100 || 0.00, [Validators.required, Validators.pattern(/^\d*$|^\d*.\d{1,2}$/)]),
      'startDate': new FormControl(moment.utc(this.contract.startDate || moment(), "YYYY-M-D"), Validators.required),
      'endDate': new FormControl(this.contract.endDate ? moment.utc(this.contract.endDate, "YYYY-M-D") : ''),
      'dailyHours': new FormControl(this.contract.dailyHours || ''),
      'laborRelation': new FormControl(this.contract.laborRelation || true),
      'workWeek': new FormControl(this.contract.workWeek || ''),
      'person': new FormControl(this.contract.person.code || ''),
      'role': new FormControl(this.contract.role.code || ''),
      'department': new FormControl(this.contract.department.code || '')
    });
  }

  setWindowData(windowData: WindowData) {
    this.windowData = windowData;
  }

  getCloseWindowId() : EventEmitter<number> {
    return this.closeWindowId;
  }

  setData(data: any) {
    if (!data) {
      this.initializeData();
    } else {
      this.contract = data;
    }
    this.person = this.contract.person;
    this.department = this.contract.department;
    this.role = this.contract.role;
  }

  selectPersonTypeAhead(person: PersonModel){
    this.form.controls.person.setValue(person.code);
    this.person = person;
  }

  selectDepartmentTypeAhead(department: DepartmentModel){
    this.form.controls.department.setValue(department.code);
    this.department = department;
  }

  selectRoleTypeAhead(role: RoleModel){
    this.form.controls.role.setValue(role.code);
    this.role = role;
  }

  public closeOnEscape(id: number) {
    if(this.form.untouched) {
      this.closeWindow();
    }
  }

  public closeWindow() {
    this.closeWindowId.emit(this.windowData.id);
  }

  public save() {
    if(!this.form.valid) return;
    if(this.contract.uuid) {
      this.contractController.update(this.contract, this.form, this.person, this.role, this.department)
        .subscribe(r=>{
          this.closeWindow();
        });
    } else {
      this.contractController.saveContract(this.contract, this.form, this.person, this.role, this.department)
      .subscribe(r=>{
        this.contract.uuid = r.uuid;
        this.closeWindow();
      });
    }
    this.form.disable();
    this.isLoading = true;
  }

  focus() {
    this.windowControllService.focusWindow(this.windowData.id);
  }

  openTypeAhead(event: Event, objCode: string, isClick: boolean) {
    if(!isClick){
      if(objCode != (<HTMLInputElement>event.target).getAttribute('formControlName')){
        return;
      }
    }
    super.openTypeAheadBase(objCode, this.windowData.id);
  }

  showErrors(form:FormGroup) {
    super.showErrorsBase(form, this.windowData.id);
  }

  private initializeData() {
    if (!this.contract) {
      this.contract = {} as ContractModel;
    }
    if (!this.contract.person) {
      this.contract.person = {} as PersonModel;
    }
    if (!this.contract.role) {
      this.contract.role = {} as RoleModel;
    }
    if (!this.contract.department) {
      this.contract.department = {} as DepartmentModel;
    }
  }
}