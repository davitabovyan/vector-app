import {Component, Output, EventEmitter, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { WorkAreaComponent } from '../../../work-area/work-area.component';
import { RoleModel } from 'src/app/shared/model/role.model';
import { RoleContrllerService } from 'src/app/shared/service/rest-api/controller/role.controller.service';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { FormService } from 'src/app/shared/service/component/form.service';
import { IconService } from 'src/app/shared/service/component/icon.service';

@Component({
    selector: 'app-role-form',
    templateUrl: './role.form.component.html',
    styleUrls: ['./role.form.component.less'],
  })
export class RoleFormComponent implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public role: RoleModel;
  public isLoading = false;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();
  
  @ViewChild('typeahead') typeahead: WorkAreaComponent;
  
  form: FormGroup;

  constructor(
    private windowControllService: WindowControllService,
    private roleController: RoleContrllerService,
    private formService: FormService,
    private iconService: IconService,
    private elementRef: ElementRef) {
      iconService.registerSpinner();
  }

  ngOnInit() {
    this.initializeData();
    this.form = new FormGroup({
      'code': new FormControl(this.role.code || '', [Validators.required, Validators.pattern(/^\d{2,6}$/)]),
      'name': new FormControl(this.role.name || '', Validators.required),
      'description': new FormControl(this.role.description || ''),
      'eligibleDays': new FormControl(this.role.eligibleDays || 0)
    });
  }

  setWindowData(windowData: WindowData) {
    this.windowData = windowData;
  }

  getCloseWindowId() : EventEmitter<number> {
    return this.closeWindowId;
  }

  setData(data: any) {
    if (!data) {
      this.initializeData();
    } else {
      this.role = data;
    }
  }

  public closeOnEscape(id: number) {
    if(this.form.untouched) {
      this.closeWindow(id);
    }
  }

  public closeWindow(id: number) {
    this.closeWindowId.emit(id);
  }

  public save() {
    if(!this.form.valid) return;
    if(this.role.uuid) {
      this.roleController.update(this.role, this.form)
        .subscribe(r=>{
          this.closeWindow(this.windowData.id);
        });
    } else {
      this.roleController.saveRole(this.role, this.form)
      .subscribe(r=>{
        this.role.uuid = r.uuid;
        this.closeWindow(this.windowData.id);
      });
    }
    this.form.disable();
    this.isLoading = true;
  }

  focus() {
    this.windowControllService.focusWindow(this.windowData.id);
  }

  private initializeData() {
    if (!this.role) {
      this.role = {} as RoleModel;
    }
    if (!this.role.code) {
      this.role.code = this.formService.getNextCode(ObjCodeEnum.ROLE);
    }
  }


}