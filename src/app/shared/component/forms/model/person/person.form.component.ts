import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';

import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { PersonModel } from 'src/app/shared/model/person.model';
import { Pair } from 'src/app/shared/model/system/pair';
import { EnumService } from 'src/app/shared/enums/enum.service';
import { PersonContrllerService } from 'src/app/shared/service/rest-api/controller/person-controller.service';
import { FormService } from 'src/app/shared/service/component/form.service';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { IconService } from 'src/app/shared/service/component/icon.service';

const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-person-form',
    templateUrl: './person.form.component.html',
    styleUrls: ['./person.form.component.less'],
  })
export class PersonFormComponent implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public isLoading = false;
  public person: PersonModel;
  public genders: Pair[];

  private readonly emailPattern = /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();
  
  form: FormGroup;

  constructor(
    private windowControllService: WindowControllService,
    private personController: PersonContrllerService,
    private formService: FormService,
    private iconService: IconService,
    private enumService: EnumService) {

  }

  ngOnInit() {
    this.genders = this.enumService.getGenders();
    if (!this.person) {
      this.person = {} as PersonModel;
    }
    if (!this.person.code) {
      this.person.code = this.formService.getNextCode(ObjCodeEnum.PERSON);
    }
    this.form = new FormGroup({
      'code': new FormControl(this.person.code || '', [Validators.required, Validators.pattern(/^\d{4,10}$/)]),
      'firstName': new FormControl(this.person.firstName || '', Validators.required),
      'lastName': new FormControl(this.person.lastName || '', Validators.required),
      'middleName': new FormControl(this.person.middleName || ''),
      'ssn': new FormControl(this.person.ssn || '', Validators.pattern(/^\d{10}$/)),
      'email': new FormControl(this.person.email || '', Validators.pattern(this.emailPattern)),
      'cell': new FormControl(this.person.cell || '', Validators.pattern(/^[()+\d\s]*$/)),
      'birthday': new FormControl(moment.utc(this.person.birthday || '', "YYYY-M-D")),
      'gender': new FormControl(this.person.gender || 'MALE')
    });
  }


  setData(data: any) {
    this.person = data;
  }

  setWindowData(windowData: WindowData) {
    this.windowData = windowData;
  }

  getCloseWindowId() : EventEmitter<number> {
    return this.closeWindowId;
  }

  public closeOnEscape(id: number) {
    if(this.form.untouched) {
      this.closeWindow(id);
    }
  }

  public closeWindow(id: number) {
    this.closeWindowId.emit(id);
  }

  public save() {
    if(!this.form.valid) return;
    if(this.person.uuid) {
      this.personController.update(this.person, this.form)
        .subscribe(r=>{
          this.closeWindow(this.windowData.id);
        });
    } else {
      this.personController.savePerson(this.person, this.form)
      .subscribe(r=>{
        this.person.uuid = r.uuid;
        this.closeWindow(this.windowData.id);
      });
    }
    this.form.disable();
    this.isLoading = true;
  }

  focus() {
    this.windowControllService.focusWindow(this.windowData.id);
  }

}