import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { TimesheetContrllerService } from 'src/app/shared/service/rest-api/controller/timesheet.controller.service';
import { IconService } from 'src/app/shared/service/component/icon.service';
import { DepartmentModel } from 'src/app/shared/model/department.model';
import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { WorkAreaComponent } from '../../../work-area/work-area.component';
import { Pair } from 'src/app/shared/model/system/pair';
import { TypeaheadModel } from 'src/app/shared/model/typeahead.model';

@Component({
    selector: 'app-timesheet-form',
    templateUrl: './timesheet.form.component.html',
    styleUrls: ['./timesheet.form.component.less'],
  })
export class TiemsheetFormComponent implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public department: DepartmentModel;
  public period: TypeaheadModel;
  // public person: PersonModel;
  public form: FormGroup;
  public isLoading = false;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild('typeahead') typeahead: WorkAreaComponent;

  constructor(
    private windowControllService: WindowControllService,
    private timesheetController: TimesheetContrllerService,
    private elementRef: ElementRef,
    private iconService: IconService,
  ) {
    iconService.registerSpinner();
  }

    ngOnInit() {
      this.form = new FormGroup({
        // 'person': new FormControl(''),
        'period': new FormControl('', Validators.required),
        'department': new FormControl('')
      });
    }

    setWindowData(windowData: WindowData) {
      this.windowData = windowData;
    }

    getCloseWindowId() : EventEmitter<number> {
      return this.closeWindowId;
    }

    setData(data: any) {

    }

    public closeOnEscape(id: number) {
      if(this.form.untouched) {
        this.closeWindow(id);
      }
    }

    public closeWindow(id: number) {
        this.closeWindowId.emit(id);
    }

    public calculate() {
        if(!this.form.valid) return;
        sessionStorage.setItem("pCurrMonth", this.period.code);
        this.timesheetController.calculateTimesheet(this.period.code, null)
          .subscribe(r=>{
            this.closeWindow(this.windowData.id);
          });
    }

    openTypeAhead(event: MouseEvent, objCode: string) {
      this.elementRef.nativeElement.querySelector('#modal-overlay-' + objCode).classList.toggle("open");
      let typeahead = this.elementRef.nativeElement.querySelector('#typeahead-' + objCode);
      typeahead.setAttribute("top",event.clientX + "px");
      typeahead.setAttribute("left",event.clientY + "px");
      typeahead.classList.toggle("open");
    }

    selectDepartmentTypeAhead(department: DepartmentModel){
      this.form.controls.department.setValue(department.code);
      this.department = department;
    }

    selectPeriodTypeAhead(period: TypeaheadModel){
      this.form.controls.period.setValue(period.code);
      this.period = period;
    }

    focus() {
        this.windowControllService.focusWindow(this.windowData.id);
    }
}