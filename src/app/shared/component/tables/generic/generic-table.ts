import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { FormArray, FormControl, AbstractControl } from '@angular/forms';
import remove from 'lodash/remove';
import find from 'lodash/find';
import map from 'lodash/map';
import times from 'lodash/times';
import constant from 'lodash/constant';

import { WindowData } from 'src/app/shared/dto/window.data';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { GenericWindow } from 'src/app/shared/dto/generic.window';
import { PageInfoData } from 'src/app/shared/dto/page.info.data';
import { TableDataService } from 'src/app/shared/service/rest-api/table.data.service';
import { ObjCodeEnum } from 'src/app/shared/enums/objcode.enum';
import { MatTableDataSource } from '@angular/material/table';
import { StorageService } from 'src/app/shared/service/component/storage.service';
import { ControllerService } from 'src/app/shared/service/rest-api/controller.service';
import { TableColumnService } from 'src/app/shared/service/table.column.service';
import { ValueTypeEnum } from 'src/app/shared/enums/value.type';
import { ConverterService } from 'src/app/shared/service/utils/converter.service';
import { IconService } from 'src/app/shared/service/component/icon.service';


@Component({
  selector: 'generic-table',
  styleUrls: ['generic-table.less'],
  templateUrl: 'generic-table.html',
})
export class GenericTable  implements OnInit, GenericWindow {
  public selectedRows = new Set();
  public footerCalc: number[];
  public activeRow: string;
  public windowData: WindowData;
  public displayedColumns: string[];
  public displayedColumnsNames: string[];
  public displayedColumnsWidths: number[];
  public displayedColumnsTypes: ValueTypeEnum[];
  public displayedColumnsSortable: boolean[];
  public allDisplayedColumns: string[];
  public allDisplayedColumnsNames: string[];
  public allDisplayedColumnWidths: number[];
  public allDisplayedColumnsTypes: ValueTypeEnum[];
  public dataSource: MatTableDataSource<any>;
  public showFilter: boolean = false;
  public showData: boolean = false;
  public selectionCriteria: string = "";
  public filterFormArray: AbstractControl[];
  public data: any[];

  @ViewChild(MatSort, {static: true})
  sort: MatSort;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  doubleClick: EventEmitter<PageInfoData> = new EventEmitter<PageInfoData>();

  constructor(
    private windowControllService: WindowControllService,
    private controllerService: ControllerService,
    private tableDataService: TableDataService,
    private tableColumnService: TableColumnService,
    private converterService: ConverterService,
    private storageService: StorageService,
    private iconService: IconService,
    private elementRef: ElementRef) {
    
      iconService.register();
  }

  ngOnInit() {
    setTimeout(() => {
      let filterInputs = ' '.repeat(this.displayedColumns.length).split(''); //workaround for dinamic array
      this.filterFormArray = new FormArray(filterInputs.map(x=>new FormControl(''))).controls;
      this.initializeDatasource();
    }, 10);
    setTimeout(() => {
      this.firstLoad();
    }, 20);
  }

  getColName(index: number): string {
    return this.displayedColumnsNames[index];
  }

  openSelectColumns(event: MouseEvent) {
    let elem = this.elementRef.nativeElement.querySelector('#select-column-'+this.windowData.id);
    elem.setAttribute("style",`top:${event.clientY - 60}px; left:${event.clientX - 420}px`);
    elem.classList.toggle("closed");
    elem.classList.toggle("open");
  }

  columnIsSelected(column:string) : boolean {
    return ( find(this.displayedColumns,  (n) => n == column) !== undefined );
  }

  applySelectColumn(column: string, isChecked:boolean) {
    if(isChecked) { 
      const index = this.allDisplayedColumns.indexOf(column);
      this.displayedColumns.splice(index, 0, column);
      this.displayedColumnsNames.splice(index, 0, this.allDisplayedColumnsNames[index]);
      this.displayedColumnsWidths.splice(index, 0, this.allDisplayedColumnWidths[index]);
      this.displayedColumnsTypes.splice(index, 0, this.allDisplayedColumnsTypes[index]);
    } else {
      const index = this.displayedColumns.indexOf(column);
      this.displayedColumns.splice(index,1);
      this.displayedColumnsNames.splice(index,1);
      this.displayedColumnsWidths.splice(index,1);
      this.displayedColumnsTypes.splice(index, 1);
    }

    this.closeSelectColumn();
  }

  closeSelectColumn() {
    let elem = this.elementRef.nativeElement.querySelector('#select-column-'+this.windowData.id);
    elem.classList.remove("open");
    elem.classList.add("closed");
  }

  applyFilter(event: Event) {
    this.dataSource.filter = (event.target as HTMLInputElement).value;
  }

  public getWindowId(): number {
    return this.windowData.id;
  }

  public closeWindow(id: number) {
    this.closeWindowId.emit(id);
  }

  public isSelected(uuid: string): boolean {
    return this.selectedRows.has(uuid); 
  }

  selectRow(uuid:string, event: KeyboardEvent) {
    
    if(uuid == this.activeRow
       && this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {

      let isSelected = this.isSelected(uuid);
      this.updateRowSelection(!isSelected, uuid);
      this.changeCheckboxState(uuid,!isSelected);
      event.stopImmediatePropagation();
    }
  }

  private changeCheckboxState(uuid:string, state:boolean) {
    this.elementRef.nativeElement.querySelector('#ch_'+uuid)['checked'] = state;
  }

  selectCheckbox(uuid:string, checked:boolean) {
    this.select(uuid);
    if(checked) {
      if(!this.isSelected(uuid)) {
        this.updateRowSelection(true, uuid);
      }
    } else {
      if(this.isSelected(uuid)) {
        this.updateRowSelection(false, uuid);
      }
    }
  }

  selectAllCheckboxes(checked:boolean, event:MouseEvent) {
    event.stopPropagation();

    if(!checked) {
      this.selectedRows.clear();
    }

    for(let i=0;i<this.data.length;i++) {
      let uuid = this.data[i].uuid;
      if(checked){
        this.updateRowSelection(checked, uuid);
      }
      this.changeCheckboxState(this.data[i].uuid, checked);
    }
  }

  private updateRowSelection(state:boolean, uuid:string) {
    if (state) {
      this.selectedRows.add(uuid);
      this.updateFooter(uuid, true);
    } else {
      this.selectedRows.delete(uuid);
      this.updateFooter(uuid, false);
    }
    this.goToNext(uuid);
  }

  selectAll(uuid:string, event: KeyboardEvent) {
    if(event) event.preventDefault();
    if(this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {
      if (this.isSelected(uuid)) {
        this.selectedRows.delete(uuid);
      } else {
        this.selectedRows.add(uuid);
      }
    }
  }

  deselectAll() {
    if(this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {
      this.selectedRows.clear();
    }
  }

  private updateFooter(uuid:string, add: boolean) {
    let record = find(this.data, {'uuid': uuid});
    for(let i=0;i< this.displayedColumnsTypes.length; i++) {
      if(this.displayedColumnsTypes[i] == ValueTypeEnum.NUMBER){
        if(add) {
          this.footerCalc[i] += record[this.displayedColumns[i]];
        } else {
          this.footerCalc[i] -= record[this.displayedColumns[i]];
        }
      }
    }
  }

  goToNext(uuid: string, event?: KeyboardEvent) {
    if(event) event.preventDefault();
    if(uuid == this.activeRow  
      && this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {

      let elem = this.elementRef.nativeElement.querySelector('.active');
      let next = elem.nextElementSibling;
      if(next) {
        elem.classList.remove("active");
        next.classList.add("active");
        this.activeRow = next.getAttribute('id');
        if(event) event.stopImmediatePropagation();
      }
    }
  }

  goToPerviouse(event: KeyboardEvent, uuid: string) {
    event.preventDefault();
    if(uuid == this.activeRow
      && this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {

      let elem = this.elementRef.nativeElement.querySelector('.active');
      let previous = elem.previousElementSibling;
      if(previous){
        elem.classList.remove("active");
        previous.classList.add("active");
        this.activeRow = previous.getAttribute('id');
        event.stopImmediatePropagation();
      }
    }
  }

  select(uuid: string) {
    this.activeRow = uuid;
  }

  editRecord(data: any, uuid?: string){
    if(uuid && this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {
      if(uuid == this.activeRow) {
        this.doubleClick.emit(new PageInfoData(this.windowData.page, data));
      }
    }
  }

  onMatSortChange(sort: Sort) {
    if (!sort.active || sort.direction === '') {
      return;
    }
    let data = this.dataSource.data.slice();
    this.dataSource.data = data.sort((a,b) => {
      const isAsc = sort.direction === 'asc';
      if(typeof a[sort.active] === 'object') {
        let left = this.converterService.stringifyObject(a[sort.active]);
        let right = this.converterService.stringifyObject(b[sort.active]);
        return (left < right ? -1 : 1)
        * (isAsc ? 1 : -1);
      } else {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
      }
    });
  }

  focus() {
    this.windowControllService.focusWindow(this.windowData.id);
  }

  // @HostListener('document:keydown.shift.k')
  taggleFilters() {
    if(this.windowControllService.isCurrentActiveWindow(this.windowData.id)) {
      for(let control of this.filterFormArray) {
        control.setValue("");
      }
      this.dataSource.filter = "";
      this.showFilter = !this.showFilter;
    }
  }

  refreash(){
    let objCode = ObjCodeEnum[this.windowData.page.split("-")[1].toUpperCase()];
    this.tableDataService.getTableData(objCode).subscribe(objects => {
      objects = this.storageService.appendDependences(objCode, objects);
      let data = this.converterService.convertData(objCode, objects);
      this.dataSource = new MatTableDataSource(data);
      this.storageService.updateTypeahead(objCode, objects);
    });
  }

  getAlign(type: ValueTypeEnum) : boolean {
    return type == ValueTypeEnum.NUMBER;
  }

  addLeftMenu() {
    this.closeLeftMenu();
    let data = {};
    this.dataSource.data.push(data);
    this.dataSource._updateChangeSubscription();
    this.doubleClick.emit(new PageInfoData(this.windowData.page, data));
  }

  copyLeftMenu() {
    let data = find(this.dataSource.data, (n) => n.uuid == this.activeRow);
    let copy = {...data};
    copy.uuid = undefined;
    let type = this.windowData.page.split("-")[1];
    this.controllerService.save(copy, ObjCodeEnum[type.toUpperCase()])
    .subscribe(r=>{
        copy.uuid = r.uuid;
        this.dataSource.data.push(copy);
        this.dataSource._updateChangeSubscription();
        this.select(copy.uuid);
      });
    this.closeLeftMenu();
  }

  editLeftMenu() {
    let data = find(this.dataSource.data, (n) => n.uuid == this.activeRow);
    this.closeLeftMenu();
    this.editRecord(data, this.activeRow);
  }

  removeLeftMenu() {
      let uuid = this.activeRow;
      let type = this.windowData.page.split("-")[1];
      this.controllerService.remove(uuid, ObjCodeEnum[type.toUpperCase()])
      .subscribe(r=>{
          remove(this.dataSource.data,  function(n) {return n.uuid == uuid; })
          this.dataSource._updateChangeSubscription();
        });
      this.closeLeftMenu();
  }

  openMenu(event: MouseEvent, uuid: string, size: number) {
    event.preventDefault();
    this.activeRow = uuid;
    let elem = this.elementRef.nativeElement.querySelector('#left-menu-'+this.windowData.id);
    elem.setAttribute("style",`top:${event.clientY - 70}px; left:${event.clientX - 180}px`);
    elem.classList.remove("closed");
    elem.classList.add("open");
  }

  public closeLeftMenu() {
    let elem = this.elementRef.nativeElement.querySelector('#left-menu-'+this.windowData.id);
    elem.classList.add("closed");
    elem.classList.remove("open");
  }

  private firstLoad() {
    let element = this.elementRef.nativeElement.querySelector('.mat-row');
    if (element != null) {
      element.classList.add("active");
      this.activeRow = element.getAttribute('id');
    }
  }

  private initializeDatasource() {
    if(this.dataSource != undefined) {
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = (data, filter) => {
        for(let i=0; i<this.displayedColumns.length; i++) {
          if(this.filterFormArray[i].value != "") {
            let cell = data[this.displayedColumns[i]];
            let content;
          
            if(typeof cell === 'object') {
              content = this.converterService.stringifyObject(cell);
            } else {
              content = '' + cell;
            }

            if(content.indexOf(this.filterFormArray[i].value) !== -1) {
              continue;
            } else {
              return false;
            }
          }
        }
        return true;
      }
      this.showData = true;
      this.activeRow = this.dataSource.data.length>0 ? this.dataSource.data[0].uuid : null;
      let objCode = ObjCodeEnum[this.windowData.page.split("-")[1].toUpperCase()];
      let allColumns = this.tableColumnService.tableColumns[objCode];
      this.allDisplayedColumns = map(allColumns, 'key');
      this.allDisplayedColumnsNames = map(allColumns, 'value');
      this.allDisplayedColumnWidths = map(allColumns, 'width');
      this.allDisplayedColumnsTypes = map(allColumns, 'type');
      this.data = this.dataSource.data;
      this.footerCalc = times(this.displayedColumns.length, constant(0));
    } else {
      setTimeout(() => {
        this.initializeDatasource();
      }, 500);
    }
  }
}