import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { FormArray, FormControl, AbstractControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { TableDataService } from 'src/app/shared/service/rest-api/table.data.service';
import { TableColumnService } from 'src/app/shared/service/table.column.service';
import { ValueTypeEnum } from 'src/app/shared/enums/value.type';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';


@Component({
  selector: 'app-typeahead',
  styleUrls: ['typeahead.less'],
  templateUrl: 'typeahead.html',
})
export class TypeAheadTable  implements OnInit {
  public type = ValueTypeEnum.STRING;
  public selectedRows = new Set();
  public activeRow: string;
  public displayedColumns: string[];
  public displayedColumnsNames: string[];
  public showFilter: boolean = false;
  public showData: boolean = false;
  public filterFormArray: AbstractControl[];
  public data: MatTableDataSource<any>;

  @ViewChild(MatSort, {static: true})
  sort: MatSort;

  @Input() objCode: string;

  @Output()
  selected: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private tableDataService: TableDataService,
    private tableColumnService: TableColumnService,
    private windowControllService: WindowControllService,
    private elementRef: ElementRef) {
    
  }

  ngOnInit() {
    let visibleColumns = this.tableColumnService.typeAheadColumns[this.objCode];
    this.displayedColumns = Array.from(visibleColumns.keys());
    this.displayedColumnsNames = Array.from(visibleColumns.values());
    setTimeout(() => {
      let filterInputs = ' '.repeat(this.displayedColumns.length).split(''); //workaround for dinamic array
      this.filterFormArray = new FormArray(filterInputs.map(x=>new FormControl(''))).controls;
      this.initializeDatasource();
    }, 10);
  }

  getColName(index: number): string {
    return this.displayedColumnsNames[index];
  }

  applyFilter(event: Event) {
    this.data.filter = (event.target as HTMLInputElement).value;
  }

  public isSelected(uuid: string): boolean {
    return this.selectedRows.has(uuid); 
  }

  // selectRow(uuid:string, event: KeyboardEvent) {
    
  //   if(uuid == this.activeRow) {

  //     if (this.isSelected(uuid)) {
  //       this.selectedRows.delete(uuid);
  //     } else {
  //       this.selectedRows.add(uuid);
  //       event.stopImmediatePropagation();
  //       this.goToNext(uuid);
  //     }
  //   }
  // }

  chooseRecord(record: any) {
    if(record.uuid == this.activeRow) {
      this.selected.emit(record);
      this.closeTypeAhead();
    }
  }


  goToNext(uuid: string, objCode: string, event?: KeyboardEvent) {
    if(event) event.preventDefault();
    if(uuid == this.activeRow && objCode == this.windowControllService.typeAhead) {
      let elem = this.elementRef.nativeElement.querySelector('.active');
      let next = elem.nextElementSibling;
      if(next) {
        elem.classList.remove("active");
        next.classList.add("active");
        this.activeRow = next.getAttribute('id');
        if(event) event.stopImmediatePropagation();
      }
    }
  }

  goToPerviouse(event: KeyboardEvent, uuid: string) {
    event.preventDefault();
    if(uuid == this.activeRow) {

      let elem = this.elementRef.nativeElement.querySelector('.active');
      let previous = elem.previousElementSibling;
      if(previous){
        elem.classList.remove("active");
        previous.classList.add("active");
        this.activeRow = previous.getAttribute('id');
        event.stopImmediatePropagation();
      }
    }
  }

  select(uuid: string) {
    this.activeRow = uuid;
  }

  openTypeAhead(event: MouseEvent) {
    let modal = this.elementRef.nativeElement.querySelector('#modal-overlay-' + this.objCode);
    modal.classList.remove("close");
    modal.classList.add("open");
    let typeahead = this.elementRef.nativeElement.querySelector('#typeahead-' + this.objCode);
    typeahead.setAttribute("top",event.clientX + "px");
    typeahead.setAttribute("left",event.clientY + "px");
    typeahead.classList.remove("close");
    typeahead.classList.add("open");
  }

  closeTypeAhead() {
    let typeahead = this.elementRef.nativeElement.querySelector('#typeahead-' + this.objCode);
    typeahead.classList.remove("open");
    typeahead.classList.add("closed");
    let modal = this.elementRef.nativeElement.querySelector('#modal-overlay-' + this.objCode);
    modal.classList.remove("open");
    modal.classList.add("closed");
  }

  taggleFilters() {
    for(let control of this.filterFormArray) {
      control.setValue("");
    }
    this.data.filter = "";
    this.showFilter = !this.showFilter;
  }

  private selectFirstRow() {
    let element = this.elementRef.nativeElement.querySelector('.mat-row');
    element.classList.add("active");
    this.activeRow = element.getAttribute('id');
  }

  private initializeDatasource() {
    if(this.data != undefined) {
      this.data.sort = this.sort;
      this.data.filterPredicate = (data, filter) => {
        for(let i=0; i<this.displayedColumns.length; i++) {
          if(this.filterFormArray[i].value != "") {
            if(data[this.displayedColumns[i]].indexOf(this.filterFormArray[i].value) !== -1) {
              continue;
            } else {
              return false;
            }
          }
        }
        return true;
      }
      this.showData = true;
      this.selectFirstRow();
    } else {
      this.data = new MatTableDataSource<any>(JSON.parse(sessionStorage.getItem(this.objCode) || ""));
      setTimeout(() => {
        this.initializeDatasource();
      }, 500);
    }
  }
}