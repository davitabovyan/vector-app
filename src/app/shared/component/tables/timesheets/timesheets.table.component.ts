import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormDataInterface } from 'src/app/shared/dto/form.data.interface';
import { WindowData } from 'src/app/shared/dto/window.data';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TimesheetModel } from 'src/app/shared/model/timesheet.model';
import { FormService } from 'src/app/shared/service/component/form.service';
import { WindowControllService } from 'src/app/shared/service/component/window.controll.service';
import { TimesheetContrllerService } from 'src/app/shared/service/rest-api/controller/timesheet.controller.service';
import { IconService } from 'src/app/shared/service/component/icon.service';

@Component({
    selector: 'app-timesheet-form',
    templateUrl: './timesheet.form.component.html',
    styleUrls: ['./timesheet.form.component.less'],
  })
export class TiemsheetsTableComponent implements OnInit, FormDataInterface{

  public windowData: WindowData;
  public timesheets: TimesheetModel[];
  public form: FormGroup;
  public isLoading = false;

  @Output()
  closeWindowId: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private windowControllService: WindowControllService,
    private timesheetController: TimesheetContrllerService,
    private formService: FormService,
    private iconService: IconService,
  ) {
    iconService.registerSpinner();
  }

    ngOnInit() {
      this.initializeData();
    }

    setWindowData(windowData: WindowData) {
      this.windowData = windowData;
    }

    getCloseWindowId() : EventEmitter<number> {
      return this.closeWindowId;
    }

    setData(data: any) {
      if (!data) {
        this.initializeData();
      } else {
        this.timesheets = data;
      }
    }

    public closeOnEscape(id: number) {
      if(this.form.untouched) {
        this.closeWindow(id);
      }
    }

    public closeWindow(id: number) {
        this.closeWindowId.emit(id);
    }

    public save() {
        if(!this.form.valid) return;

    }

    focus() {
        this.windowControllService.focusWindow(this.windowData.id);
    }

    private initializeData() {
        if (!this.timesheets) {
          this.timesheets = [];
        }
    }
}