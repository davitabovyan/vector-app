import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, ElementRef} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatTableDataSource } from '@angular/material/table';

import map from 'lodash/map';
import filter from 'lodash/filter';
import forEach from 'lodash/forEach';

import { TopMenuService } from "../../service/top-manue.service";
import { HostDirective } from '../../directive/host.directive';
import { WindowService } from '../../service/component/window.service';
import { WindowControllService } from '../../service/component/window.controll.service';
import { PageInfoData } from '../../dto/page.info.data';
import { TableDataService } from '../../service/rest-api/table.data.service';
import { ObjCodeEnum } from '../../enums/objcode.enum';
import { TableColumnService } from '../../service/table.column.service';
import { StorageService } from '../../service/component/storage.service';
import { TableDataParams } from '../../dto/table.data.params';
import { ValueTypeEnum } from '../../enums/value.type';
import { ConverterService } from '../../service/utils/converter.service';

@Component({
  selector: 'app-work-area',
  templateUrl: './work-area.component.html',
  styleUrls: ['./work-area.component.less']
})
export class WorkAreaComponent implements OnInit {
  private readonly PATH = 'assets/svg/icon';

  useHandle = true;
  inBounds = true;
  edge = {
    top: true,
    bottom: true,
    left: true,
    right: true
  };

  // used instead of global data
  data: object[];

  public loadingInfo: string;

  @Input() page: string[];

  @ViewChild(HostDirective, {static: true}) ihost: HostDirective;

  constructor(
    private topMenuService: TopMenuService,
    private tableColumnService: TableColumnService,
    private windowService: WindowService,
    private windowControllService: WindowControllService,
    private tableDataService: TableDataService,
    private storageService: StorageService,
    private converterService: ConverterService,
    private elementRef: ElementRef,
    private iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
    private componentFactoryResolver: ComponentFactoryResolver) {

    this.data = this.topMenuService.menuItems;
    iconRegistry.addSvgIcon('window-close', sanitizer.bypassSecurityTrustResourceUrl(`${this.PATH}/window-close.svg`));
  }

  ngOnInit() {
    this.loadMainData();
  }

  checkEdge(event) {
    this.edge = event;
  }

  loadComponentWithData(pageInfo: PageInfoData) {
    const windowData = this.windowService.getFormWindowDataByKey(pageInfo.page);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(windowData.component);
    const viewContainerRef = this.ihost.viewContainerRef;
    const componentRef = viewContainerRef.createComponent(componentFactory, windowData.index);

    componentRef.instance.setData(pageInfo.data);
    componentRef.instance.setWindowData(windowData);
    componentRef.instance.getCloseWindowId().subscribe(v=> this.closeWindow(v));
    
    this.focusWindow(windowData.id);
  }

  loadComponent(page: string, params?: TableDataParams) {
    switch (page.split("-")[0]) {
      case 'new':
        this.loadComponentWithData(new PageInfoData(page, null));
        break;
      case 'table':
        this.loadTableComponent(page, params);
        break;
      case 'refresh':
        this.loadMainData();
        break;
      default:
        break;
    }
  }

  loadTableComponent(page: string, params?: TableDataParams) {
    const windowData = this.windowService.getTableWindowDataByKey(page);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(windowData.component);
    const viewContainerRef = this.ihost.viewContainerRef;
    const componentRef = viewContainerRef.createComponent(componentFactory, windowData.index);

    let objCode = page.split("-")[1];
    let data : any[];

    windowData.page = `new-${objCode}`;
    // if(windowData.size == WindowSize.RELATIV) {
    //   windowData.size = WindowSize['COL_' + visibleColumns.size];
    // }

    let objectEnum = ObjCodeEnum[objCode.toUpperCase()];
    this.tableDataService.getTableData(objectEnum, params).subscribe(objects => {
      objects = this.storageService.appendDependences(objectEnum, objects);
      this.setDisplayColumns(objCode, componentRef, objects);
      let data = this.converterService.convertData(objCode, objects);
      componentRef.instance.dataSource = new MatTableDataSource(data);

      this.storageService.updateTypeahead(objectEnum, objects);
      data = objects;
    });
    this.setDisplayColumns(objCode, componentRef, this.data);
    componentRef.instance.windowData = windowData;
    componentRef.instance.selectionCriteria = this.tableDataService.getSelectionCriteria(objectEnum);
    componentRef.instance.closeWindowId.subscribe(v=> this.closeWindow(v));
    componentRef.instance.doubleClick.subscribe(v=> this.loadComponentWithData(v));

    this.focusWindow(windowData.id);
  }

  private setDisplayColumns(objCode: string, componentRef: any, data: any[]) {
    let columns = this.applyColumns(objCode, data);
    componentRef.instance.displayedColumns = map(columns, 'key');
    componentRef.instance.displayedColumnsNames = map(columns, 'value');
    componentRef.instance.displayedColumnsWidths = map(columns, 'width');
    componentRef.instance.displayedColumnsTypes = map(columns, 'type');
    componentRef.instance.displayedColumnsSortable = map(columns, 'sortable');
  }

  private applyColumns(objCode: string, data: any[]) : any[] {
    let columns = filter(this.tableColumnService.tableColumns[objCode],{default: true});
    switch (objCode) {
      case ObjCodeEnum.TIMESHEET:
        if(data.length>0) {
          forEach(data[0]["hours"], function(value, key) {
            let column = key.split("-")[2];
            columns.push({key:'day' + column, value: column, width: 0, default: true, type: ValueTypeEnum.DAY, sortable: false});
          });
        }
        break;

      default: 
    }
    return columns;
  }

  public closeWindow(id: number) {
    const index = this.windowService.getOpenWindowIndexForRemove(id);
    this.ihost.viewContainerRef.remove(index);
  }

  public loadMainData(){
    let i = 0;
    let count = 3;
    this.elementRef.nativeElement.querySelector('#loading-info').classList.toggle("closed");

    this.loadInitialData(ObjCodeEnum.PERSON, "Աշխատակիցների", ++i === count);
    this.loadInitialData(ObjCodeEnum.ROLE, "Պաշտոնների", ++i === count);
    this.loadInitialData(ObjCodeEnum.DEPARTMENT, "Բաժինների", ++i === count);
    this.storageService.createPayrollPeriods();
  }

  private loadInitialData(objCode: ObjCodeEnum, message: string, isLast: boolean){
    this.loadingInfo = message + " ցանկը բեռնվում է ․․․";
    this.tableDataService.getTypeAheadData(objCode).subscribe(objects => {
      sessionStorage.setItem(objCode, JSON.stringify(objects));
      this.loadingInfo = message + " ցանկը բեռնվեց";
      if(isLast) {
        this.loadingInfo = "";
        this.elementRef.nativeElement.querySelector('#loading-info').classList.toggle("closed");
      }
    });
  }

  private focusWindow(id: number) {
    setTimeout(() => {
      this.windowControllService.focusWindow(id);
    }, 10);
  }

}
